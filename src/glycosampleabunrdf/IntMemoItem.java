package glycosampleabunrdf;

import java.util.LinkedList;

public class IntMemoItem {
	private String int_localIds = "";
	private String internal = "";

	public IntMemoItem(String a_int_localIds
			,String a_internal) {
		this.int_localIds = a_int_localIds;
		this.internal = a_internal;

	}

	public String get_int_localIds() {
		return this.int_localIds;
	}

	public String get_internal() {
		return this.internal;
	}



}
