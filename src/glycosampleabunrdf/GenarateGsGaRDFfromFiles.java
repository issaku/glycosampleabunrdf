package glycosampleabunrdf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;

public class GenarateGsGaRDFfromFiles {

	/*
	 *
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Start RDF generation ...");

		Integer count = 1;

		// AH-20
		// Disease: AH-19 cancer
		// Normal:  AH-18 normal
		Boolean bL_test = true;
		bL_test = false;
		//String data_ext = ".txt";
		String data_ext = "xtsa";
		//String data_ext = "tsam";
		if (bL_test) {
			data_ext = ".tes";
		}

		String t_strFilePath = "";
		String t_strDirPath = "/Users/yamada/Desktop/BH18.7/GlycoNAVI_RDF/GD/";
		//t_strFilePath += "Disease_PMID18214858_三善先生_膵臓癌.txt";
		//t_strFilePath = t_strDirPath + "Disease_PMID- 16385567_三善先生_膵臓癌ほか.txt";
		String t_strRDFDirPath = "/Users/yamada/Desktop/BH18.7/GlycoNAVI_RDF/RDF/";

		for (int i=0; i<args.length; ++i) {
			if ("-f".equals(args[i])) {
				t_strDirPath = args[++i];
			}
		}

		Prefix pre = new Prefix();
        Date dt = new Date();
        SimpleDateFormat dtf = new SimpleDateFormat("-yyyy-MM-dd-HH-mm");
        String dtStr = dtf.format(dt);

    	// Setting for Write ttl file of GlycoSample
    	String diseaseSamplePath = t_strRDFDirPath + "DiseaseSample" + dtStr + ".ttl";
        FileWriter writeGSfile = new FileWriter(diseaseSamplePath, true);
        PrintWriter pwGS = new PrintWriter(new BufferedWriter(writeGSfile));

        //pwGS.println(pre.getGlycoSamplePrefix());

    	// Setting for Write ttl file of GlycoAbun
    	String diseaseAbunPath = t_strRDFDirPath + "DiseaseAbun" + dtStr + ".ttl";
        FileWriter writeDAfile = new FileWriter(diseaseAbunPath, true);
        PrintWriter pwDA = new PrintWriter(new BufferedWriter(writeDAfile));

        //pwGA.println(pre.getGlycoAbunPrefix());

        //DataSplit datasp = new DataSplit();
		LinkedList<GSGAbunString> entities;
		SourceData o_sd = new SourceData("");
		DiseaseSample o_ds = new DiseaseSample("");
		DiseaseAbun o_ga = new DiseaseAbun("");
		String dS_ID = "DS_";
		String dA_ID = "DA_";
		String GD_ID = "GD_";
		String Zero_id = "";
		//Map<String, String> map = new HashMap<>();
		//HashMap<String, String> map = new HashMap<String, String>();
		LinkedList<IdKeyValue> idkv = new LinkedList<IdKeyValue>();



	try {
		File dir = new File(t_strDirPath);
	    File[] files = dir.listFiles();


	    for (File file : files ) {
			if (file.exists()) {
				Path t_path = file.toPath();
				String t_fileName = t_path.getFileName().toString();
				//System.out.println(t_path);
				String t_ext = "";
				if (t_fileName.length() > 3) {
					t_ext = t_fileName.substring(t_fileName.length() - 4, t_fileName.length());
				}
				//System.out.println(t_ext);
				if(t_fileName != null && t_ext.equals(data_ext)) {
					//System.out.println("file exist.");
					String t_filePath = file.toPath().toString();
					System.out.println(t_filePath);
					DataFileReader reader = new DataFileReader(t_filePath);
					String str_FileStrings = reader.getFileStrings();
					str_FileStrings = util.replaceCharacter(str_FileStrings);

					// split string: source, sample, abundance
					DataSplit datasp = new DataSplit();
					entities = datasp.dataSplit(str_FileStrings);

	//				DataSplit datasp = new DataSplit();
	//				LinkedList<GSGAbunString> entities = datasp.dataSplit(str_FileStrings);
	//				SourceData o_sd;
	//				GlycoSample o_gs;
	//				GlycoAbun o_ga;

					//Integer count = 1;
					for (GSGAbunString entity : entities) {


						// make GS ID


						if (o_ds.get_glycosampleId() == "") {
							dS_ID = "DS_" + count;
							dA_ID = "DA_" + count;
							GD_ID = "GD_" + count;
							Zero_id = String.format("%07d", count);
						}
						else {
							dA_ID = dS_ID.replace("DS_", "DA_");
							GD_ID = dS_ID.replace("DS_", "GD_");
							Zero_id = String.format("%07d", dS_ID.replace("DS_", "GD_"));
						}


						count++;
						//System.out.println("GS_ID: "+ GS_ID);
						//System.out.println("GA_ID: "+ GA_ID);

						// generate test RDF data for visualization
						//if (count == 1) break;

						//System.out.println("------ entity ---------");
						// Source
						String str_source = entity.getSourceData();
						//System.out.println(str_source);
						o_sd = new SourceData(str_source);
						//System.out.println(o_sd.printText());
						// GlycoSample
						String str_sample = entity.getSampleData();
						o_ds = new DiseaseSample(str_sample);
						//System.out.println(o_gs.getlocalId());
						//o_gs.printText();
						// Glycoconjugate infomation
						String str_abun = entity.getAbunData();
						o_ga = new DiseaseAbun(str_abun);
						//System.out.println("----- Abundance Set -------");
						for (LinkedList<Abundance> abun : o_ga.getSet()) {
							//System.out.println("----- Abundance -------");
							for (Abundance ab : abun) {
								//ab.printText();
							}
						}







						// write RDF data to files

						// GlycoSample START
						// https://qiita.com/cloudysunny14@github/items/46ecd0fa5dba09290746

						// create an empty Model
						Model ds_model = ModelFactory.createDefaultModel();

						// add Prefix
						ds_model.setNsPrefix(PrefixTerm.DS.getPrefix(), PrefixTerm.DS.getUri());
						ds_model.setNsPrefix(PrefixTerm.GN.getPrefix(), PrefixTerm.GN.getUri());
						ds_model.setNsPrefix(PrefixTerm.SIO.getPrefix(), PrefixTerm.SIO.getUri());
						ds_model.setNsPrefix(PrefixTerm.OBO.getPrefix(), PrefixTerm.OBO.getUri());
						ds_model.setNsPrefix(PrefixTerm.GA.getPrefix(), PrefixTerm.GA.getUri());
						ds_model.setNsPrefix(PrefixTerm.GD.getPrefix(), PrefixTerm.GD.getUri());
						ds_model.setNsPrefix(PrefixTerm.SKOS.getPrefix(), PrefixTerm.SKOS.getUri());
						ds_model.setNsPrefix(PrefixTerm.RDFS.getPrefix(), PrefixTerm.RDFS.getUri());
						ds_model.setNsPrefix(PrefixTerm.RDF.getPrefix(), PrefixTerm.RDF.getUri());
						ds_model.setNsPrefix(PrefixTerm.DCTERMS.getPrefix(), PrefixTerm.DCTERMS.getUri());
						ds_model.setNsPrefix(PrefixTerm.id_pubmed.getPrefix(), PrefixTerm.id_pubmed.getUri());
						ds_model.setNsPrefix(PrefixTerm.pubmed.getPrefix(), PrefixTerm.pubmed.getUri());



























						// generate hashed id
	                    // String glycosample_id = $doi_pubmed.$doi_pubmed_id.$data_source.$sample_count.$local_ID);
						String diseasesample_label =
								"DiseaseSample "
								+ "DOI:" + o_sd.get_doi() + " "
								+ "PMID:" + o_sd.get_pmid() + " "
								+ "ISBN:" + o_sd.get_isbn() + " "
								+ o_sd.get_sourceSite() + " "
								+ o_ds.getlocalId() + " "
								+ o_ds.get_createDate();
						//glycosample_id = glycosample_id.replaceAll(" ", "+").replaceAll("\n", "+");
						// String glycosample_hased_id = util.getSHA1string(glycosample_id);
						String EntityResource = PrefixTerm.DS.getUri() + dS_ID;

						// create the resource
						Resource diseasesample = ds_model.createResource(EntityResource);

						// add the property
						String t_type = ClassUri.Sample.getClassUri(); // PrefixTerm.GS.getUri() + "Sample";
						Resource r_type = ds_model.createResource(t_type);
						diseasesample.addProperty(RDF.type, r_type);
						diseasesample.addProperty(DCTerms.identifier, dS_ID);
						diseasesample.addProperty(ds_model.createProperty(PredicateTerms.id.getPredicate()), "DS" + Zero_id);
						diseasesample.addProperty(RDFS.label, diseasesample_label);
						diseasesample.addProperty(ds_model.createProperty(PredicateTerms.altLabel.getPredicate()), o_ds.getlocalId());
						diseasesample.addProperty(DCTerms.title, util.replaceCharacter(o_ds.get_sampleTitle()));
						if (o_ds.get_createDate() != "" && o_ds.get_createDate() != null) {
							diseasesample.addProperty(DCTerms.created, o_ds.get_createDate());
						}
						String t_localIdUri = PrefixTerm.DS.getUri() + o_ds.getlocalId();
						Resource t_localIdRes = ds_model.createResource(t_localIdUri);
						diseasesample.addProperty(ds_model.createProperty(PredicateTerms.exactMatch.getPredicate()), t_localIdRes);

						// PMID
						if (o_sd.get_pmid() != "" && o_sd.get_pmid() != null) {
							//System.out.println("PMID: " + o_sd.get_pmid());
							String t_PMIDUri = PrefixTerm.pubmed.getUri() + o_sd.get_pmid();
							Resource t_PMIDUriRes = ds_model.createResource(t_PMIDUri);
							diseasesample.addProperty(DCTerms.references, t_PMIDUriRes);
							String id_PmidRes = PrefixTerm.id_pubmed.getUri() + o_sd.get_pmid();
							Resource t_id_PMIDUriRes = ds_model.createResource(id_PmidRes);
							t_PMIDUriRes.addProperty(RDFS.seeAlso, t_id_PMIDUriRes);
							t_PMIDUriRes.addProperty(DCTerms.identifier, o_sd.get_pmid());
							t_id_PMIDUriRes.addProperty(DCTerms.identifier, o_sd.get_pmid());
						}


						// Description
						if (o_ds.get_sampleDescription() != "" && o_ds.get_sampleDescription() != null) {
							diseasesample.addProperty(DCTerms.description, util.replaceCharacter(o_ds.get_sampleDescription()));
						}

						// Disease

						//if (o_gs.get_disease() != "" && o_gs.get_disease() != null) {
						if (o_ds.get_disease() != "") {
							System.out.println(GD_ID + "\t" + o_ds.get_disease());

							String Disease = PrefixTerm.GD.getUri() + GD_ID;
							// create the resource
							Resource DiseaseResource = ds_model.createResource(Disease);
							String DiseaseClassString = ClassUri.Disease.getClassUri();
							Resource DiseaseClass = ds_model.createResource(DiseaseClassString);
							DiseaseResource.addProperty(RDF.type, DiseaseClass);
							DiseaseResource.addProperty(DCTerms.identifier, GD_ID);
							diseasesample.addProperty(ds_model.createProperty(PredicateTerms.disease.getPredicate()), DiseaseResource);
							DiseaseResource.addProperty(RDFS.label, o_ds.get_disease());

							if (o_ds.get_doId() != "" && o_ds.get_doId() != null) {

								DiseaseResource.addProperty(ds_model.createProperty(PredicateTerms.has_doid.getPredicate()), o_ds.get_doId());
							}
							if (o_ds.get_mesh() != "" && o_ds.get_mesh() != null) {

								DiseaseResource.addProperty(ds_model.createProperty(PredicateTerms.has_mesh.getPredicate()), o_ds.get_mesh());
							}
						}


						// Health_state
						if (o_ds.getHealth_state() != "" && o_ds.getHealth_state() != null) {
							String t_Health_stateUri = PrefixTerm.DS.getUri() + o_ds.getHealth_state();
							Resource t_Health_stateUriRes = ds_model.createResource(t_Health_stateUri);
							diseasesample.addProperty(ds_model.createProperty(PredicateTerms.has_Health_state.getPredicate()), t_Health_stateUriRes);
							String id_Health_statRes = PrefixTerm.id_pubmed.getUri() + o_ds.getHealth_state();
							Resource t_id_PMIDUriRes = ds_model.createResource(id_Health_statRes);
							t_id_PMIDUriRes.addProperty(RDFS.label, o_ds.getHealth_state());
						}



						// AA sequence
						if (o_ds.get_proteinSequence() != "" && o_ds.get_proteinSequence() != null) {
							diseasesample.addProperty(ds_model.createProperty(PredicateTerms.AA_Seq.getPredicate()), o_ds.get_proteinSequence());
							//System.out.println(o_gs.get_proteinSequence());
						}
						// obo:NCIT_C25447
						// key value

						for (SampleKeyValue kv : o_ds.get_keyvalueData()) {
							Resource bl_1 = ds_model.createResource();
							diseasesample.addProperty(ds_model.createProperty(PredicateTerms.NCIT_C25447.getPredicate()), bl_1);
							Resource bl_k = ds_model.createResource();
							bl_1.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYTYPE.getPredicate()), bl_k);
							bl_k.addProperty(RDFS.label, kv.getKey());
							Resource bl_v = ds_model.createResource();
							bl_1.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYVALUE.getPredicate()), bl_v);
							bl_v.addProperty(RDFS.label, util.replaceCharacter(kv.getValue()));


							// TODO:
							String Organism = "";
							String Taxonomy_id = "";
							String Gene_Symbol = "";
							// UniProtID->Seq	">sp|P00738|HPT_HUMAN Haptoglobin OS=Homo sapiens OX=9606 GN=HP PE=1 SV=1
							//if (kv.getKey().equals("UniProtID->Seq")) {
							if (kv.getKey().equals("FASTA")) {

								// split >sp



								String[] or1 = kv.getValue().split("OS=");
								if (or1.length == 2) {
									String[] or2 = or1[1].split("OX=");
									if (or2.length == 2) {
										Organism = or2[0].trim();
										String[] or3 = or2[1].split("GN=");
										if (or3.length == 2) {
											Taxonomy_id = or3[0].trim();
											String[] or4 = or3[1].split("PE=");
											if (or4.length == 2) {
												Gene_Symbol = or4[0].trim();
												//String[] or4 = or3[1].split("PE=");
											}
										}
									}
								}
							}

							if (Organism == "") {
								if (Taxonomy_id.equals(Tax.HUMAN.getId())){
									Organism = Tax.HUMAN.getOrganism();
								}
							}


							if (Organism != "") {
								diseasesample.addProperty(ds_model.createProperty(PredicateTerms.organism.getPredicate()), Organism);

								Resource bl_o = ds_model.createResource();
								diseasesample.addProperty(ds_model.createProperty(PredicateTerms.NCIT_C25447.getPredicate()), bl_o);
								Resource bl_ko = ds_model.createResource();
								bl_o.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYTYPE.getPredicate()), bl_ko);
								bl_ko.addProperty(RDFS.label, "Organism (UniProt)");
								Resource bl_vo = ds_model.createResource();
								bl_o.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYVALUE.getPredicate()), bl_vo);
								bl_vo.addProperty(RDFS.label, Organism);
							}

							if (Taxonomy_id != "") {
								diseasesample.addProperty(ds_model.createProperty(PredicateTerms.taxonomy.getPredicate()), Taxonomy_id);

								Resource bl_t = ds_model.createResource();
								diseasesample.addProperty(ds_model.createProperty(PredicateTerms.NCIT_C25447.getPredicate()), bl_t);
								Resource bl_kt = ds_model.createResource();
								bl_t.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYTYPE.getPredicate()), bl_kt);
								bl_kt.addProperty(RDFS.label, "Taxonomic identifier (UniProt)");
								Resource bl_vt = ds_model.createResource();
								bl_t.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYVALUE.getPredicate()), bl_vt);
								bl_vt.addProperty(RDFS.label, Taxonomy_id);
							}

							if (Gene_Symbol != "") {
								diseasesample.addProperty(ds_model.createProperty(PredicateTerms.gene.getPredicate()), Gene_Symbol);

								Resource bl_g = ds_model.createResource();
								diseasesample.addProperty(ds_model.createProperty(PredicateTerms.NCIT_C25447.getPredicate()), bl_g);
								Resource bl_kg = ds_model.createResource();
								bl_g.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYTYPE.getPredicate()), bl_kg);
								bl_kg.addProperty(RDFS.label, "Gene (UniProt)");
								Resource bl_vg = ds_model.createResource();
								bl_g.addProperty(ds_model.createProperty(PredicateTerms.PROPERTYVALUE.getPredicate()), bl_vg);
								bl_vg.addProperty(RDFS.label, Gene_Symbol);
							}




						}


						// GlycoSample END


						// GlycoAbun START


			            // create an empty Model
			            Model da_model = ModelFactory.createDefaultModel();

						// add Prefix
			            da_model.setNsPrefix(PrefixTerm.DS.getPrefix(), PrefixTerm.DS.getUri());
			            da_model.setNsPrefix(PrefixTerm.SIO.getPrefix(), PrefixTerm.SIO.getUri());
			            da_model.setNsPrefix(PrefixTerm.OBO.getPrefix(), PrefixTerm.OBO.getUri());
			            da_model.setNsPrefix(PrefixTerm.GA.getPrefix(), PrefixTerm.GA.getUri());
			            da_model.setNsPrefix(PrefixTerm.SKOS.getPrefix(), PrefixTerm.SKOS.getUri());
			            da_model.setNsPrefix(PrefixTerm.RDFS.getPrefix(), PrefixTerm.RDFS.getUri());
			            da_model.setNsPrefix(PrefixTerm.RDF.getPrefix(), PrefixTerm.RDF.getUri());
			            da_model.setNsPrefix(PrefixTerm.DCTERMS.getPrefix(), PrefixTerm.DCTERMS.getUri());
			            da_model.setNsPrefix(PrefixTerm.GCO.getPrefix(), PrefixTerm.GCO.getUri());
			            da_model.setNsPrefix(PrefixTerm.UP.getPrefix(), PrefixTerm.UP.getUri());
			            da_model.setNsPrefix(PrefixTerm.UNIPROT.getPrefix(), PrefixTerm.UNIPROT.getUri());
			            da_model.setNsPrefix(PrefixTerm.EDAM.getPrefix(), PrefixTerm.EDAM.getUri());
			            da_model.setNsPrefix(PrefixTerm.GD.getPrefix(), PrefixTerm.GD.getUri());
			            da_model.setNsPrefix(PrefixTerm.GN.getPrefix(), PrefixTerm.GN.getUri());
			            da_model.setNsPrefix(PrefixTerm.GP.getPrefix(), PrefixTerm.GP.getUri());
			            da_model.setNsPrefix(PrefixTerm.SET.getPrefix(), PrefixTerm.SET.getUri());
			            da_model.setNsPrefix(PrefixTerm.SETITEM.getPrefix(), PrefixTerm.SETITEM.getUri());
			            da_model.setNsPrefix(PrefixTerm.GLYCAN.getPrefix(), PrefixTerm.GLYCAN.getUri());
			            da_model.setNsPrefix(PrefixTerm.GTCI.getPrefix(), PrefixTerm.GTCI.getUri());
			            da_model.setNsPrefix(PrefixTerm.GTC.getPrefix(), PrefixTerm.GTC.getUri());
			            da_model.setNsPrefix(PrefixTerm.FOAF.getPrefix(), PrefixTerm.FOAF.getUri());
			            da_model.setNsPrefix(PrefixTerm.FALDO.getPrefix(), PrefixTerm.FALDO.getUri());
			            da_model.setNsPrefix(PrefixTerm.CODAO.getPrefix(), PrefixTerm.CODAO.getUri());
			            da_model.setNsPrefix(PrefixTerm.pubmed.getPrefix(), PrefixTerm.pubmed.getUri());
			            da_model.setNsPrefix(PrefixTerm.id_pubmed.getPrefix(), PrefixTerm.id_pubmed.getUri());
			            da_model.setNsPrefix(PrefixTerm.XSD.getPrefix(), PrefixTerm.XSD.getUri());





			            // generate hashed id
			            String diseaseabun_label =
			            		"DiseaseAbundance "
			            	+ "DOI:" + o_sd.get_doi() + " "
			                + "PMID:" + o_sd.get_pmid() + " "
			                + "ISBN:" + o_sd.get_isbn() + " "
			                + o_sd.get_sourceSite() + " "
			                + o_ds.getlocalId() + " "
			                + o_ds.get_createDate();
			            //glycoabun_id = glycoabun_id.replaceAll(" ", "+").replaceAll("\n", "+");
			            // String glycoabun_hased_id = util.getSHA1string(glycoabun_id);
			            String AbunEntityResource = PrefixTerm.GA.getUri() + dA_ID;

			            // create the resource
			            Resource glycoabun = da_model.createResource(AbunEntityResource);

			            // link GlycoSample and GlycoAbun Resources
			            // TODO: gs_model or ga_model ?
//			            glycosample.addProperty(gs_model.createProperty(PredicateTerms.has_Abundance.getPredicate()), glycoabun);
//			            glycoabun.addProperty(ga_model.createProperty(PredicateTerms.has_Sample.getPredicate()), glycosample);

			            // gs_model sample and abundance
			            glycoabun.addProperty(da_model.createProperty(PredicateTerms.has_Sample.getPredicate()), diseasesample);
			            diseasesample.addProperty(ds_model.createProperty(PredicateTerms.has_Abundance.getPredicate()), glycoabun);
			            // ga_model sample and abundance
			            glycoabun.addProperty(da_model.createProperty(PredicateTerms.has_Sample.getPredicate()), diseasesample);
			            diseasesample.addProperty(ds_model.createProperty(PredicateTerms.has_Abundance.getPredicate()), glycoabun);






			            // add the property
			            String t_a_type = ClassUri.Abundance.getClassUri(); // PrefixTerm.GA.getUri() + "Resource";
			            Resource r_a_type = da_model.createResource(t_a_type);
			            glycoabun.addProperty(RDF.type, r_a_type);
			            glycoabun.addProperty(DCTerms.identifier, dA_ID);
			            glycoabun.addProperty(da_model.createProperty(PredicateTerms.id.getPredicate()), "DA" + Zero_id);
			            glycoabun.addProperty(RDFS.label, diseaseabun_label);
			            glycoabun.addProperty(da_model.createProperty(PredicateTerms.altLabel.getPredicate()), o_ds.getlocalId());


			            // add referenced entity
			            String t_a_entityUri = PrefixTerm.GA.getUri() + dA_ID + "_ReferenceEntity";
			            Resource r_a_referenceEntity = da_model.createResource(t_a_entityUri);
			            r_a_referenceEntity.addProperty(RDFS.label, "ReferenceEntity of " + dA_ID);
			            glycoabun.addProperty(da_model.createProperty(PredicateTerms.has_Ref_Comp.getPredicate()), r_a_referenceEntity);

						String t_abunlocalIdUri = PrefixTerm.GA.getUri() + o_ds.getlocalId();
						Resource t_abunlocalIdRes = ds_model.createResource(t_abunlocalIdUri);
						glycoabun.addProperty(ds_model.createProperty(PredicateTerms.exactMatch.getPredicate()), t_abunlocalIdRes);





			            // Disease
			            if (o_ds.get_disease() != "") {
				            String str_Disease_Association = PrefixTerm.GD.getUri() + util.getUrlString(dA_ID + "_DiseaseAssociation");
				            Resource r_a_Disease_Association = da_model.createResource(str_Disease_Association);
				            r_a_Disease_Association.addProperty(RDFS.label, "Disease_Association of " + dA_ID);

				            String str_CompoundDiseaseAssosiation = ClassUri.CompoundDiseaseAssosiation.getClassUri();
				            Resource r_a_CompoundDiseaseAssosiation = da_model.createResource(str_CompoundDiseaseAssosiation);
				            r_a_Disease_Association.addProperty(RDF.type, r_a_CompoundDiseaseAssosiation);
				            r_a_referenceEntity.addProperty(da_model.createProperty(PredicateTerms.has_association.getPredicate()), r_a_Disease_Association);

				            String str_Disease = PrefixTerm.GD.getUri() + util.getUrlString(o_ds.get_disease());
				            Resource r_o_Disease = da_model.createResource(str_Disease);
				            r_a_Disease_Association.addProperty(da_model.createProperty(PredicateTerms.refers_to.getPredicate()), r_o_Disease);
				            r_o_Disease.addProperty(RDFS.label, o_ds.get_disease());

				            if (o_ds.get_doId() != "") {
					            r_o_Disease.addProperty(SKOS.notation, o_ds.get_doId());
					            String str_DiseaseDOID = PrefixTerm.OBO.getUri() + util.getUrlString(o_ds.get_doId());
					            Resource r_o_DiseaseDOID = da_model.createResource(str_DiseaseDOID);
					            r_o_Disease.addProperty(RDF.type, r_o_DiseaseDOID);
					            r_o_Disease.addProperty(RDFS.seeAlso, r_o_DiseaseDOID);
				            }
			            }

			            // Source
			            String str_Source = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Source");
			            Resource r_a_Source = da_model.createResource(str_Source);
			            r_a_referenceEntity.addProperty(da_model.createProperty(PredicateTerms.is_from_source.getPredicate()), r_a_Source);
			            String str_Source_natural = ClassUri.Source_natural.getClassUri();
			            Resource r_a_Source_natural = da_model.createResource(str_Source_natural);
			            r_a_Source.addProperty(RDF.type, r_a_Source_natural);
			            r_a_Source.addProperty(RDFS.label, "Source of " + dA_ID);

			            if (o_ds.get_cellLien() != "") {
			            	r_a_Source.addProperty(da_model.createProperty(PredicateTerms.has_cell_line.getPredicate()), o_ds.get_cellLien());
			            }
			            if (o_ds.get_tissue() != "") {
			            	r_a_Source.addProperty(da_model.createProperty(PredicateTerms.has_tissue.getPredicate()), o_ds.get_tissue());
			            }

			            if (o_ds.getTaxId() != "") {
				            String str_Taxon = PrefixTerm.GA.getUri() + util.getUrlString("Taxon_" + o_ds.getTaxId());
				            Resource r_a_Taxon = da_model.createResource(str_Taxon);
				            r_a_Source.addProperty(da_model.createProperty(PredicateTerms.has_taxon.getPredicate()), r_a_Taxon);
				            String UP_taxon = ClassUri.Taxon.getClassUri();
				            Resource r_a_upTaxon = da_model.createResource(UP_taxon);
				            r_a_Taxon.addProperty(RDF.type, r_a_upTaxon);
			            	r_a_Taxon.addProperty(RDFS.label, "Taxid:"+ o_ds.getTaxId());
			            }



			            // Citation
			            String str_Citation = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Citation");
			            Resource r_a_Citation = da_model.createResource(str_Citation);
			            r_a_Citation.addProperty(RDFS.label, "Citation of " + dA_ID);
			            r_a_referenceEntity.addProperty(da_model.createProperty(PredicateTerms.published_in.getPredicate()), r_a_Citation);
			            String str_CitationClass = ClassUri.Citation.getClassUri();
			            Resource r_a_CitationClass = da_model.createResource(str_CitationClass);
			            r_a_Citation.addProperty(RDF.type, r_a_CitationClass);

			            // pmid
			            if (o_sd.get_pmid() != "") {
				            String str_pmidUri = PrefixTerm.pubmed.getUri() + util.getUrlString(o_sd.get_pmid());
				            Resource r_a_PMID = da_model.createResource(str_pmidUri);
				            r_a_PMID.addProperty(DCTerms.identifier, o_sd.get_pmid());
				            r_a_PMID.addProperty(RDFS.label, "PMID:" + o_sd.get_pmid());
				            r_a_Citation.addProperty(RDFS.seeAlso, r_a_PMID);
				            //dcterms:references
				            r_a_Citation.addProperty(DCTerms.references, r_a_PMID);

				            String str_id_pmidUri = PrefixTerm.id_pubmed.getUri() + util.getUrlString(o_sd.get_pmid());
				            Resource r_a_id_PMID = da_model.createResource(str_id_pmidUri);
				            r_a_id_PMID.addProperty(DCTerms.identifier, o_sd.get_pmid());
				            r_a_id_PMID.addProperty(RDFS.label, "PMID:" + o_sd.get_pmid());
				            r_a_Citation.addProperty(RDFS.seeAlso, r_a_id_PMID);
				            r_a_Citation.addProperty(DCTerms.references, r_a_id_PMID);
			            }






			            // Protein entity
		            	String t_a_RefProteinEntityUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_ReferencedProtein");
			            Resource r_a_referencedProteinEntity = da_model.createResource(t_a_RefProteinEntityUri);
			            r_a_referencedProteinEntity.addProperty(RDFS.label, "Referenced Protein of " + dA_ID);

			            if (o_ds.isProtein()) {
			            	r_a_referenceEntity.addProperty(da_model.createProperty(PredicateTerms.has_Protein_Part.getPredicate()), r_a_referencedProteinEntity);

		            		String t_a_ProteinEntityUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Protein");
		            		Resource r_a_ProteinEntity = da_model.createResource(t_a_ProteinEntityUri);
		            		r_a_ProteinEntity.addProperty(RDFS.label, "Protein of " + dA_ID);
		            		r_a_referencedProteinEntity.addProperty(da_model.createProperty(PredicateTerms.has_Protein.getPredicate()), r_a_ProteinEntity);
		            		r_a_ProteinEntity.addLiteral(da_model.createProperty(PredicateTerms.amino_acid_length.getPredicate()), o_ds.get_aminoAcidLength());
		            		Resource GlycoproteinClass = da_model.createResource(ClassUri.Glycoprotein.getClassUri());
		            		r_a_ProteinEntity.addProperty(RDF.type, GlycoproteinClass);
		            		if (o_ds.get_proteinSequence().length() > 0) {
		            			r_a_ProteinEntity.addProperty(da_model.createProperty(PredicateTerms.AA_Seq.getPredicate()), o_ds.get_proteinSequence());
							}




		            		// UniProt
		            		if (o_ds.get_uniprot() != null) {
				            	if (o_ds.get_uniprot().size() > 0) {

				            		for (UniProt up : o_ds.get_uniprot()) {

			            				for (String id : up.getUniProtIds()) {
						            		// https://www.uniprot.org/uniprot/Q966M4
						            		String t_a_UniProtEntityUri = PrefixTerm.UNIPROT.getUri() + util.getUrlString(id);
						            		Resource r_a_UniProtEntity = da_model.createResource(t_a_UniProtEntityUri);
						            		r_a_UniProtEntity.addProperty(RDFS.label, "UniProt of " + dA_ID);
						            		//r_a_referencedProteinEntity.addProperty(ga_model.createProperty(PredicateTerms.has_Protein.getPredicate()), r_a_UniProtEntity);
						            		r_a_ProteinEntity.addProperty(RDFS.seeAlso, r_a_UniProtEntity);
						            		Resource ProteinClass = da_model.createResource(ClassUri.Protein.getClassUri());
						            		r_a_UniProtEntity.addProperty(RDF.type, ProteinClass);
						            		r_a_UniProtEntity.addProperty(DCTerms.identifier, id);
						            		//amino_acid_length

						            		// part or not
					            			if (up.getBl_part()) {
					            				r_a_UniProtEntity.addLiteral(da_model.createProperty(PredicateTerms.is_part.getPredicate()), up.getBl_part());
					            			}
					            			else {

					            			}

						            		// mod or not
					            			if (up.getBl_mod()) {
					            				r_a_UniProtEntity.addLiteral(da_model.createProperty(PredicateTerms.has_mod.getPredicate()), up.getBl_mod());
					            			}
					            			else {

					            			}


			            				}
				            		}
				            	}
		            		}




			            }





		            	// abundance information
			            LinkedList<LinkedList<Abundance>> t_set = o_ga.getSet();
			            Integer set_count = 0;
			            for (LinkedList<Abundance> list : t_set) {
			            	set_count++;
			            	String abunSetResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Entity" + "_Set_" + set_count);

			            	//String glycoabun_hased_set_id = util.getSHA1string(abunSetResourceUri);
			            	Resource tr_set = da_model.createResource(abunSetResourceUri);
			            	tr_set.addProperty(RDFS.label, "Set " + set_count + " of " + dA_ID);
			            	r_a_referencedProteinEntity.addProperty(da_model.createProperty(PredicateTerms.has_Saccharide_Set.getPredicate()), tr_set);
			            	Resource SetClass = da_model.createResource(ClassUri.Set.getClassUri());
		            		tr_set.addProperty(RDF.type, SetClass);

			            	Integer setitem_count = 1;
			            	for (Abundance abun : list) {
			            		//String getGlycanString = abun.getGlycan();
			            		//System.out.println(getGlycanString);

			            		// Region
					            String t_a_RegionEntityUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Region_" + abun.getPosition());
			            		Resource r_a_RegionEntity = da_model.createResource(t_a_RegionEntityUri);
			            		r_a_RegionEntity.addProperty(RDFS.label, "Region " + abun.getPosition() + " of " + dA_ID);
			            		r_a_RegionEntity.addProperty(da_model.createProperty(PredicateTerms.has_Saccharide_Set.getPredicate()), tr_set);
			            		r_a_referencedProteinEntity.addProperty(da_model.createProperty(PredicateTerms.glycosylated_at.getPredicate()), r_a_RegionEntity);
			            		Resource Glycosylation_SiteClass = da_model.createResource(ClassUri.Glycosylation_Site.getClassUri());
			            		r_a_RegionEntity.addProperty(RDF.type, Glycosylation_SiteClass);

			            		// position
			            		String t_a_PositionEntityUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Position_" + abun.getPosition());
			            		Resource r_a_PositionEntity = da_model.createResource(t_a_PositionEntityUri);
			            		r_a_RegionEntity.addProperty(da_model.createProperty(PredicateTerms.location.getPredicate()), r_a_PositionEntity);

			            		Pattern patt = Pattern.compile("^[0-9]+$");
			            		Matcher matc = patt.matcher(abun.getIntPosition().toString());
			            		if (matc.matches()) {
			            			Resource ExactPositionClass = da_model.createResource(ClassUri.ExactPosition.getClassUri());
			            			r_a_PositionEntity.addProperty(RDF.type, ExactPositionClass);

				            		r_a_PositionEntity.addProperty(da_model.createProperty(PredicateTerms.position.getPredicate()), abun.getIntPosition());
				            		r_a_PositionEntity.addProperty(RDFS.label, "Position:" + abun.getPosition() + " of " + dA_ID);
				            		r_a_PositionEntity.addProperty(SKOS.altLabel, abun.getIntPosition());
				            		// amino acid
				            		String t_a_GlycosylatedAAEntityUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID+ "_Position_" + abun.getPosition() + "_AminoAcid_" + abun.getAttached_position_AA());
				            		Resource r_a_GlycosylatedAAEntity = da_model.createResource(t_a_GlycosylatedAAEntityUri);
				            		r_a_PositionEntity.addProperty(da_model.createProperty(PredicateTerms.has_amino_acid.getPredicate()), r_a_GlycosylatedAAEntity);
				            		//r_a_GlycosylatedAAEntity(RDF.type, ) // Glycosylated_AA
				            		Resource Glycosylated_AAClass = da_model.createResource(ClassUri.Glycosylated_AA.getClassUri());
				            		r_a_GlycosylatedAAEntity.addProperty(RDF.type, Glycosylated_AAClass);
				            		r_a_GlycosylatedAAEntity.addProperty(da_model.createProperty(PredicateTerms.has_Value.getPredicate()), abun.getAttached_position_AA());
				            		r_a_GlycosylatedAAEntity.addProperty(RDFS.label, dA_ID + ": Glycosylated amino acid (" + abun.getPosition() + ")");


			            		}
			            		else {
			            			Resource FuzzyPositionClass = da_model.createResource(ClassUri.FuzzyPosition.getClassUri());
			            			r_a_PositionEntity.addProperty(RDF.type, FuzzyPositionClass);
			            			r_a_PositionEntity.addProperty(SKOS.altLabel, abun.getPosition());
			            		}




			            		// rdfs:seeAlso uniprot position


			            		if (abun.getLocal_glycan().length() > 0) {
				            		String abunResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Entity" + "_Set_" + set_count + "_SetItem_" + setitem_count);
				            		Resource tr_setitem = da_model.createResource(abunResourceUri);
				            		Resource SetItemClass = da_model.createResource(ClassUri.SetItem.getClassUri());
				            		tr_setitem.addProperty(RDF.type, SetItemClass);

				            		tr_set.addProperty(da_model.createProperty(PredicateTerms.has_SetItem.getPredicate()), tr_setitem);
				            		String SteItemResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Entity" + "_Set_" + set_count + "_SetItem_" + setitem_count + "_Glycan_" + abun.getLocal_glycan());
				            		String GlycanResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Entity" + "_Saccharide_" + abun.getLocal_glycan());
				            		Resource GlycanResource = da_model.createResource(GlycanResourceUri);
				            		Resource SteItemResource = da_model.createResource(SteItemResourceUri);
				            		SteItemResource.addProperty(da_model.createProperty(PredicateTerms.exactMatch.getPredicate()), GlycanResource);
				            		GlycanResource.addProperty(da_model.createProperty(PredicateTerms.exactMatch.getPredicate()), SteItemResource);

				            		// abundance
				            		String SteItemAbundanceResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Entity" + "_Set_" + set_count + "_SetItem_" + setitem_count + "_Glycan_" + abun.getLocal_glycan()) + "_Abundance";
				            		Resource SteItemAbundanceResource = da_model.createResource(SteItemAbundanceResourceUri);
				            		Resource Relative_abundanceClass = da_model.createResource(ClassUri.Relative_abundance.getClassUri());
				            		SteItemAbundanceResource.addProperty(RDF.type, Relative_abundanceClass);

				            		SteItemResource.addProperty(da_model.createProperty(PredicateTerms.relative_abundance_percentage.getPredicate()), SteItemAbundanceResource);
				            		SteItemAbundanceResource.addProperty(da_model.createProperty(PredicateTerms.has_Value.getPredicate()), abun.getAbundanceRatio());

				            		Pattern p = Pattern.compile("^[0-9\\.]+$");
				            		Matcher m = p.matcher(abun.getAbundanceRatio());
				            		if (m.matches()) {
				            			SteItemAbundanceResource.addProperty(RDFS.comment, "Relative abundance ratio (" + abun.getLocal_glycan() + "): " + abun.getAbundanceRatio() + abun.getAbundanceType());
				            			SteItemAbundanceResource.addProperty(RDFS.label, abun.getAbundanceRatio() + abun.getAbundanceType());
				            		}
				            		else {
				            			SteItemAbundanceResource.addProperty(RDFS.comment, "Relative abundance ratio (" + abun.getLocal_glycan() + "): " + abun.getAbundanceRatio());
				            			SteItemAbundanceResource.addProperty(RDFS.label, abun.getAbundanceRatio());
				            		}

				            		// abundance unit :TODO

				            		setitem_count++;


				            		// ^G[0-9]{5}[A-Z]{2}$
				            		//Pattern pat = Pattern.compile("^G[0-9]{5}[A-Z]{2}$");
				            		//Matcher mat = pat.matcher(abun.getGlycan());
				            		//if (mat.matches()) {
					            		tr_setitem.addProperty(da_model.createProperty(PredicateTerms.is_Glycan.getPredicate()), SteItemResource);
					            		SteItemResource.addProperty(RDFS.label, "Glycan:" + abun.getLocal_glycan());
					            		SteItemResource.addProperty(DCTerms.identifier, abun.getLocal_glycan());
				            		//}
				            		//else {
					            		//tr_setitem.addProperty(ga_model.createProperty(PredicateTerms.is_GlycanType.getPredicate()), SteItemResource);
					            		//SteItemResource.addProperty(RDFS.label, abun.getGlycan());

				            		//}
			            		}
			            	}
			            }
			            //}

			            // Lipid entity




			            // Saccharide entity

		            	String t_a_RefSaccharideEntityUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_ReferencedSaccharide");
			            Resource r_a_referencedSaccharideEntity = da_model.createResource(t_a_RefSaccharideEntityUri);
		            	r_a_referenceEntity.addProperty(da_model.createProperty(PredicateTerms.has_Saccharide_Part.getPredicate()), r_a_referencedSaccharideEntity);

			            // Reference Protein  -> Reference Saccahride
			            if (o_ds.isProtein()) {
			            	r_a_referencedProteinEntity.addProperty(da_model.createProperty(PredicateTerms.has_Attached_Referenced_Saccharide.getPredicate()), r_a_referencedSaccharideEntity);
			            }


			         // glycan information
			            LinkedList<LinkedList<Abundance>> t_Saccharides = o_ga.getSet();
			            for (LinkedList<Abundance> list : t_Saccharides) {
			            	for (Abundance abun : list) {
			            		// TODO: 2018-10-16
			            		if (abun.getGlycan().length() > 0) {
				            		//String GlycanResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(GA_ID + "_Entity" + "_Saccharide_" + abun.getGlycan());
			            			String Local_GlycanResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(dA_ID + "_Entity" + "_Saccharide_" + abun.getLocal_glycan());
				            		Resource Local_GlycanResource = da_model.createResource(Local_GlycanResourceUri);
				            		r_a_referencedSaccharideEntity.addProperty(da_model.createProperty(PredicateTerms.has_Glycan.getPredicate()), Local_GlycanResource);
				            		Resource SaccharideClass = da_model.createResource(ClassUri.Saccharide.getClassUri());
				            		Local_GlycanResource.addProperty(RDF.type, SaccharideClass);

				            		// ^G[0-9]{5}[A-Z]{2}$
				            		Pattern p = Pattern.compile("^G[0-9]{5}[A-Z]{2}$");
				            		Matcher m = p.matcher(abun.getGlycan());
				            		if (m.matches()) {
				            			Local_GlycanResource.addProperty(RDFS.label, "id:" + abun.getLocal_glycan());
				            			Local_GlycanResource.addProperty(DCTerms.identifier, abun.getLocal_glycan());
					            		// GTC
					            		String GTCResourceUri = PrefixTerm.GTC.getUri() + util.getUrlString(abun.getGlycan());
					            		Resource GTCResource = da_model.createResource(GTCResourceUri);
					            		GTCResource.addProperty(DCTerms.identifier, abun.getGlycan());
					            		Local_GlycanResource.addProperty(da_model.createProperty(PredicateTerms.primaryTopic.getPredicate()), GTCResource);
					            		// GTC identifiers.org
					            		String IdenGTCResourceUri = PrefixTerm.GTCI.getUri() + util.getUrlString(abun.getGlycan());
					            		Resource IdenGTCResource = da_model.createResource(IdenGTCResourceUri);
					            		IdenGTCResource.addProperty(DCTerms.identifier, abun.getGlycan());
					            		GTCResource.addProperty(RDFS.seeAlso, IdenGTCResource);


				            		}
				            		else {
				            			Local_GlycanResource.addProperty(RDFS.label, abun.getLocal_glycan());

				            		}
			            		}
			            	}
			            }
			            // GlycoAbun END


						// DB Memo :TODO

			         // hash map
						IdKeyValue t_idkv = new IdKeyValue();
						t_idkv.setKeyValue(dA_ID, o_ds.getlocalId());
						idkv.add(t_idkv);


						//System.out.println("#"+ GA_ID + "\t" + o_gs.getlocalId() + " memo" );
						for (Memo mem : o_ga.getMemos()) {
							for (DbMemoItem mi : mem.get_DbMemoItems() ) {

								String str_localId_string = mi.get_db_localIds().replaceAll(",", "_").replaceAll(" ", "");
								for (IdKeyValue kv : idkv) {
									str_localId_string = str_localId_string.replaceAll(kv.getValue(), kv.getKey());
								}

								//System.out.println(str_localId_string);
								//System.out.println(mi.get_db_Edited());

								String DbMemoResourceUri = PrefixTerm.GPSS.getUri() + util.getUrlString(str_localId_string);
			            		Resource DbMemoResource = da_model.createResource(DbMemoResourceUri);

			            		for (String t_id : mi.get_db_localId_List()) {
			            			String t_DA_ID = "";
			            			for (IdKeyValue kv : idkv) {
			            				if (kv.getValue().equals(t_id)) {
			            					t_DA_ID = kv.getKey();
			            				}
			            			}
				            		String DbMemoAbunEntityResource = PrefixTerm.GA.getUri() + t_DA_ID;
						            Resource db_memo_glycoabun = da_model.createResource(DbMemoAbunEntityResource);
						            db_memo_glycoabun.addProperty(da_model.createProperty(PredicateTerms.has_DB_Memo.getPredicate()), DbMemoResource);
								}

								Resource DbMemoClass = da_model.createResource(ClassUri.DB_memo.getClassUri());
								DbMemoResource.addProperty(RDF.type, DbMemoClass);
								DbMemoResource.addProperty(da_model.createProperty(PredicateTerms.has_Value.getPredicate()), mi.get_db_Edited());


							}
							//System.out.println(mem.getdDiscussionMemo_Edited().size());
						}




			            /*
			            for(String val : map.values()){
			            	System.out.println("val\t" + val);
			            }



			            for(Entry<String, String> entry : map.asMap().entrySet()){
			    		    String t_ga_id = entry.getKey();
			    		    String t_local_id = entry.getValue();
			    		    System.out.println("check\t" + t_ga_id + "\t" + t_local_id);

			    		    for (Memo mem : o_ga.getMemos()) {
								for (String[] ids : mem.getDbMemo_localIds()) {
									for (String id : ids) {
										System.out.println(GA_ID + "\t" + id + "\tequals\t" + t_local_id);
										if (id.equals(t_local_id)){

										}
									}
								}
			    		    }
			    		}


						for (Memo mem : o_ga.getMemos()) {

							for (String[] ids : mem.getDbMemo_localIds()) {
								for (String id : ids) {
									//System.out.println(GA_ID + "\t" + id + "\tequals\t" + o_gs.getlocalId());
									if (id.equals(o_gs.getlocalId())){
										//System.out.println(GA_ID + "\t" + id + "\tequals\t" + o_gs.getlocalId());
									}
								}
							}

							if (mem.get_dbMemo_localIds_string().contains(o_gs.getlocalId())){
								//System.out.println(GA_ID + "\t" + mem.get_dbMemo_localIds_string() + "\tcontains\t" + o_gs.getlocalId());
							}




							for (DbMemoItem m : mem.get_DbMemoItems()) {
								//for (String[] ids : mem.getDbMemo_localIds()) {
								//	for (String id : ids) {
										//if (m.get_db_localIds().contains(id) || m.get_db_localIds().contains("all")) {
								//System.out.println(GA_ID);
								//System.out.println("o_gs.getlocalId()\t" + o_gs.getlocalId());
								//System.out.println("m.get_db_localIds()\t" + m.get_db_localIds());

								//System.out.println("length: " + m.get_db_localId_List().length);

								if (m.get_db_localIds().contains(o_gs.getlocalId())) {
									//System.out.println(GA_ID + "\t" + m.get_db_localIds() + "\tcontains\t" + o_gs.getlocalId());
								}

								for (String str : m.get_db_localId_List()) {
									if (str.trim().equals(o_gs.getlocalId())) {
										//System.out.println(GA_ID + "\t" + str + "\tequals\t" + o_gs.getlocalId());
									}
								}


								if (m.get_db_localIds().contains(o_gs.getlocalId())) {
											//System.out.println(GA_ID + "\t" + m.get_db_localIds() + "\tcontsins\t" + o_gs.getlocalId());

											//LinkedList<DbMemoItem> items = mem.get_DbMemoItems();
											String DbMemoResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(m.get_db_localIds());
						            		Resource DbMemoResource = ga_model.createResource(DbMemoResourceUri);

						            		//String memoAbunEntityResource = PrefixTerm.GA.getUri() + GA_ID;
								            //Resource memo_glycoabun = ga_model.createResource(memoAbunEntityResource);

											glycoabun.addProperty(ga_model.createProperty(PredicateTerms.has_DB_Memo.getPredicate()), DbMemoResource);
											Resource DbMemoClass = ga_model.createResource(ClassUri.DB_memo.getClassUri());
											DbMemoResource.addProperty(RDF.type, DbMemoClass);
											DbMemoResource.addProperty(ga_model.createProperty(PredicateTerms.has_Value.getPredicate()), m.get_db_Edited());
								}
									//}
								//}

								if (m.get_db_localIds().indexOf(o_gs.getlocalId()) != -1 || m.get_db_localIds().indexOf("all") != -1) {
									LinkedList<DbMemoItem> items = mem.get_DbMemoItems();
									String DbMemoResourceUri = PrefixTerm.GA.getUri() + util.getUrlString(m.get_db_localIds());
				            		Resource DbMemoResource = ga_model.createResource(DbMemoResourceUri);
									glycoabun.addProperty(ga_model.createProperty(PredicateTerms.has_DB_Memo.getPredicate()), DbMemoResource);
									Resource DbMemoClass = ga_model.createResource(ClassUri.DB_memo.getClassUri());
									DbMemoResource.addProperty(RDF.type, DbMemoClass);
									DbMemoResource.addProperty(ga_model.createProperty(PredicateTerms.has_Value.getPredicate()), m.get_db_Edited());
								}

							}
						}
						*/










						// https://jena.apache.org/documentation/io/rdf-output.html
						ds_model.write(pwGS, "TTL");
						da_model.write(pwDA, "TTL");

					} // entities
				} // file is *.txt
			} // file.exists
		} // for files

			// close PrintWriter
			pwGS.close();
			pwDA.close();


		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Fin...!");
	}
}
