package glycosampleabunrdf;

import java.util.LinkedList;

public class DisMemoItem {
	private String discussion_localIds = "";
	private String discussion_Edited = "";
	private String discussion_Original = "";

	public DisMemoItem(String a_discussion_localIds
			,String a_discussion_Edited
			,String a_discussion_Original
			) {
		this.discussion_localIds = a_discussion_localIds;
		this.discussion_Edited = a_discussion_Edited;
		this.discussion_Original = a_discussion_Original;
	}


	public String get_discussion_localIds() {
		return this.discussion_localIds;
	}

	public String get_discussion_Edited() {
		return this.discussion_Edited;
	}

	public String get_discussion_Original() {
		return this.discussion_Original;
	}


}
