package glycosampleabunrdf;

public class SourceData {

	private String a_str_sourceSite = "";
	private String a_str_pmid = "";
	private String a_str_doi = "";
	private String a_str_isbn = "";

	public SourceData(String a_SouceData) {
		if (a_SouceData != null) {
			// split lines
			String[] lines = a_SouceData.split("\n");

			for (int i=0; i<lines.length; ++i) {
				// PaperID
				if (lines[i].contains("PaperID")) {
					String[] data = lines[i].split("\t");
					if (data.length > 2) {
						if (data[1].contains("PMID")) {
							a_str_pmid = data[2].trim();
						}
						if (data[1].contains("DOI")) {
							a_str_doi = data[2].trim();
						}
						if (data[1].contains("ISBN")) {
							a_str_isbn = data[2].trim();
						}
					}
				}
				// Source
				if (lines[i].contains("Source")) {
					String[] data = lines[i].split("\t");
					if (data.length > 1) {
						for (int j=1; j<data.length; ++j) {
							a_str_sourceSite += data[j] + "\n";
						}
					}
				}
			}
		}
	}

	public String get_sourceSite(){
		return this.a_str_sourceSite;
	}
	public String get_pmid(){
		return this.a_str_pmid;
	}
	public String get_doi(){
		return this.a_str_doi;
	}
	public String get_isbn(){
		return this.a_str_isbn;
	}

	public String printText() {
		String str_out = "PMID:\t"+ this.a_str_pmid + "\n";
		str_out += "DOI:\t"+ this.a_str_doi + "\n";
		str_out += "ISDN:\t"+ this.a_str_isbn + "\n";
		str_out += "Source:\t"+ this.a_str_sourceSite + "\n";
		return str_out;
	}



}
