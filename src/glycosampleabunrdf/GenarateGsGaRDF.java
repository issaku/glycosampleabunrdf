package glycosampleabunrdf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.*;

public class GenarateGsGaRDF {

	/*
	 *
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Start RDF generation ...");

		String t_strFilePath = "";
		t_strFilePath = "/Users/yamada/Desktop/BH18.7/GlycoNAVI_RDF/";
		//t_strFilePath += "Disease_PMID18214858_三善先生_膵臓癌.txt";
		//t_strFilePath += "Disease_PMID- 16385567_三善先生_膵臓癌ほか.txt";
		t_strFilePath += "Disease_PMID- 16385567_三善先生_膵臓癌ほか-2018-10-15.txt";


		for (int i=0; i<args.length; ++i) {
			if ("-f".equals(args[i])) {
				t_strFilePath = args[++i];
			}
		}

		try {

			Prefix pre = new Prefix();

	        Date dt = new Date();
	        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd_");
	        String dtStr = dtf.format(dt);

	    	// Setting for Write ttl file of GlycoSample
	    	String GlycoSamplePath = t_strFilePath + dtStr + ".GlycoSample.ttl";
	        FileWriter writeGSfile = new FileWriter(GlycoSamplePath, true);
	        PrintWriter pwGS = new PrintWriter(new BufferedWriter(writeGSfile));

	        //pwGS.println(pre.getGlycoSamplePrefix());

	    	// Setting for Write ttl file of GlycoAbun
	    	String GlycoAbunPath = t_strFilePath + ".GlycoAbun.ttl";
	        FileWriter writeGAfile = new FileWriter(GlycoAbunPath, true);
	        PrintWriter pwGA = new PrintWriter(new BufferedWriter(writeGAfile));

	        //pwGA.println(pre.getGlycoAbunPrefix());

			File file = new File(t_strFilePath);
			if (file.exists()) {
				//System.out.println("file exist.");
				DataFileReader reader = new DataFileReader(t_strFilePath);
				String str_FileStrings = reader.getFileStrings();
				str_FileStrings = util.replaceCharacter(str_FileStrings);

				// split string: source, sample, abundance
				DataSplit datasp = new DataSplit();
				LinkedList<GSGAbunString> entities = datasp.dataSplit(str_FileStrings);
				SourceData o_sd;
				DiseaseSample o_gs;
				DiseaseAbun o_ga;

				Integer count = 1;
				for (GSGAbunString entity : entities) {

					// make GS ID
					String GS_ID = "GS_" + count;
					String GD_ID = "GD_" + count;
					count++;

					//System.out.println("------ entity ---------");
					// Source
					String str_source = entity.getSourceData();
					//System.out.println(str_source);
					o_sd = new SourceData(str_source);
					//System.out.println(o_sd.printText());
					// GlycoSample
					String str_sample = entity.getSampleData();
					o_gs = new DiseaseSample(str_sample);
					//System.out.println(o_gs.getlocalId());
					//o_gs.printText();
					// Glycoconjugate infomation
					String str_abun = entity.getAbunData();
					o_ga = new DiseaseAbun(str_abun);
					//System.out.println("----- Abundance Set -------");
					for (LinkedList<Abundance> abun : o_ga.getSet()) {
						//System.out.println("----- Abundance -------");
						for (Abundance ab : abun) {
							//ab.printText();
						}
					}
					for (Memo mem : o_ga.getMemos()) {

					}

					// write to files
					try {
						// GlycoSample START
						// https://qiita.com/cloudysunny14@github/items/46ecd0fa5dba09290746

						// create an empty Model
						Model gs_model = ModelFactory.createDefaultModel();

						// generate hashed id
	                    //String glycosample_id = $doi_pubmed.$doi_pubmed_id.$data_source.$sample_count.$local_ID);
						String glycosample_id =
								o_sd.get_doi()
								+ o_sd.get_pmid()
								+ o_sd.get_isbn()
								+ o_sd.get_sourceSite()
								+ o_gs.getlocalId()
								+ o_gs.get_createDate();
						glycosample_id = glycosample_id.replaceAll(" ", "+").replaceAll("\n", "+");
						String glycosample_hased_id = util.getSHA1string(glycosample_id);
						String EntityResource = PrefixTerm.DS.getUri() + glycosample_hased_id;

						// create the resource
						Resource glycosample = gs_model.createResource(EntityResource);

						// add the property
						String t_type = PrefixTerm.DS.getUri() + "Sample";
						Resource r_type = gs_model.createResource(t_type);
						glycosample.addProperty(RDF.type, r_type);
						glycosample.addProperty(DCTerms.identifier, GS_ID);
						glycosample.addProperty(RDFS.label, glycosample_id);
						glycosample.addProperty(DCTerms.title, o_gs.get_sampleTitle());
						if (o_gs.get_createDate() != "" && o_gs.get_createDate() != null) {
							glycosample.addProperty(DCTerms.created, o_gs.get_createDate());
						}
						if (o_gs.get_sampleDescription() != "" && o_gs.get_sampleDescription() != null) {
							glycosample.addProperty(DCTerms.description, o_gs.get_sampleDescription());
						}
						// Disease

						//if (o_gs.get_disease() != "" && o_gs.get_disease() != null) {
						if (o_gs.get_disease() != "") {
							System.out.println(GD_ID + "\t" + o_gs.get_disease());

							String Disease = PrefixTerm.GD.getUri() + GD_ID;
							// create the resource
							Resource DiseaseResource = gs_model.createResource(Disease);
							DiseaseResource.addProperty(RDF.type, ClassUri.Disease.getClassUri());
							DiseaseResource.addProperty(DCTerms.identifier, GD_ID);
							glycosample.addProperty(gs_model.createProperty(PredicateTerms.disease.getPredicate()), DiseaseResource);
							DiseaseResource.addProperty(RDFS.label, o_gs.get_disease());

							if (o_gs.get_doId() != "" && o_gs.get_doId() != null) {
								DiseaseResource.addProperty(gs_model.createProperty(PredicateTerms.has_doid.getPredicate()), o_gs.get_doId());
							}
						}

						//  Health_state




						// obo:NCIT_C25447
						// key value
						for (SampleKeyValue kv : o_gs.get_keyvalueData()) {
							Resource bl_1 = gs_model.createResource();
							glycosample.addProperty(gs_model.createProperty(PredicateTerms.NCIT_C25447.getPredicate()), bl_1);
							Resource bl_k = gs_model.createResource();
							bl_1.addProperty(gs_model.createProperty(PredicateTerms.PROPERTYTYPE.getPredicate()), bl_k);
							bl_k.addProperty(RDFS.label, kv.getKey());
							Resource bl_v = gs_model.createResource();
							bl_1.addProperty(gs_model.createProperty(PredicateTerms.PROPERTYVALUE.getPredicate()), bl_v);
							bl_v.addProperty(RDFS.label, kv.getValue());
						}

						// GlycoSample END





						// https://jena.apache.org/documentation/io/rdf-output.html
						//model.write(System.out, "TTL");
						//gs_model.write(pwGS, "NT");
						gs_model.write(pwGS, "TTL");
						//RDFDataMgr.write(pwGA, model, Lang.TTL);
						//.write(OutputStream, model, Lang.TTL) ;

						pwGS.println("");

						pwGA.println("pineapple");

					} catch (Exception e) {
			            e.printStackTrace();
			        }



				}
			}

			// close PrintWriter
			pwGS.close();
			pwGA.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Fin...!");
	}
}
