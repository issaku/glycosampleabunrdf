package glycosampleabunrdf;

public class IdKeyValue {
		private String str_key = "";
		private String str_value = "";

		public void setKeyValue(String str_key, String str_value){
			this.str_key = str_key;
			this.str_value = str_value;
		}

		public String getKey() {
			return this.str_key;
		}

		public String getValue() {
			return this.str_value;
		}


		public void print() {
			System.out.println(this.str_key + "\t" + this.str_value);
		}
}
