package glycosampleabunrdf;

import java.util.LinkedList;


public class DiseaseSample {

	private String glycosampleId = "";
	private String localId = "";
	//private String date = "";
	private String taxId = "";
	private String cellLien = "";
	private String doId = "";
	private String mesh = "";
	private String disease = "";
	private String tissue = "";
	private String createDate = "";
	private LinkedList<UniProt> uniprotId;
	private String proteinName = "";
	private Integer aminoAcidLength = 0;
	private String proteinSequence = "";
	private String sampleTitle = "";
	private String sampleDescription = "";
	private LinkedList<SampleKeyValue> keyvalueData = new LinkedList<SampleKeyValue>();
	private Boolean is_protein = false;
	private Boolean is_lipid = false;
	private String health_state = "";



	public DiseaseSample(String str_GlycoSampleData) {
		// separate to lines
		String[] lines = str_GlycoSampleData.split("\n");
		// check tab, tab"{data}"tab

		// remove \n and \" => datalines
		boolean bl_double_quotation = false;
		String str_dataline = "";
		LinkedList<String> datalines = new LinkedList<String>();
		for (int i=0; i<lines.length; ++i) {
			//str_dataline = lines[i];
			if (lines[i].contains("\"")) {

				str_dataline += lines[i].replaceAll("\"", "");

				if (bl_double_quotation == false) {
					bl_double_quotation = true;
				}
				else {
					datalines.add(str_dataline);
					str_dataline = "";
					bl_double_quotation = false;
				}
			}
			else {

				if (bl_double_quotation == false) {
					str_dataline += lines[i];
					datalines.add(str_dataline);
					str_dataline = "";
				}
				else {
					if (str_dataline.contains("UniProtID->Seq")) {
						str_dataline += lines[i];
						this.is_protein = true;
					}
					else {
						str_dataline += lines[i] + " ";
					}
				}
			}
		}



		for (String line: datalines){
			//System.out.println(line);
			String[] contents = line.split("\t");
			if (contents.length > 1) {
				SampleKeyValue kv = new SampleKeyValue();

				if (!contents[0].toLowerCase().contains("internal")) {
					if (contents[0].contains("UniProtID->Seq")) {
						kv.setKeyValue("FASTA", contents[1]);
					}
					else {
						kv.setKeyValue(contents[0], contents[1]);
					}

					this.keyvalueData.add(kv);
				}

				// glycosampleId
				if (contents[0].equals("GlycoSampleID")) {
					this.glycosampleId = contents[1];
					//System.out.println(this.localId);
				}

				// localId
				if (contents[0].equals("local ID")) {
					this.localId = contents[1];
					//System.out.println(this.localId);
				}
				// date
				if (contents[0].equals("Date")) {
					this.createDate = contents[1];
					//System.out.println(this.date);
				}
				// taxId
				if (contents[0].equals(GlycoSampleTerm.TAXONOMY_ID.getTerm())) {
					this.taxId = contents[1];
					//System.out.println(this.taxId);
				}
				// cellLien
				if (contents[0].equals(GlycoSampleTerm.CELL_LINE.getTerm())) {
					this.cellLien = contents[1];
					//System.out.println(this.cellLien);
				}
				// doId
				if (contents[0].equals("DOID")) {
					this.doId = contents[1];
					//System.out.println(this.doId);
				}
				// MESH
				if (contents[0].equals("MESH")) {
					this.mesh = contents[1];
					//System.out.println(this.doId);
				}
				// disease
				if (contents[0].equals(GlycoSampleTerm.DISEASE.getTerm())) {
					this.disease = contents[1];
					//System.out.println(this.desease);
				}
				//Health_state HEALTH_STATE
				if (contents[0].equals(GlycoSampleTerm.HEALTH_STATE.getTerm())) {
					this.health_state = contents[1];
					//System.out.println(this.desease);
				}

				// tissue
				if (contents[0].equals(GlycoSampleTerm.TISSUE_TYPE.getTerm())) {
					this.tissue = contents[1];
					//System.out.println(this.tissue);
				}
				// createDate
				// uniprotId
				if (contents[0].equals("Uniprot ID")) {
					UniProtList upL = new UniProtList(contents[1]);
					this.uniprotId = upL.getUniProt_Data();



					this.is_protein = true;
					//System.out.println(this.uniprotId);
				}
				// proteinName
				if (contents[0].equals("Protein Name")) {
					this.proteinName = contents[1];
					this.is_protein = true;
					//System.out.println(this.proteinName);
				}
				// AA sequence
				// TODO: multi FASTA
				if (contents[0].equals("UniProtID->Seq")) {
					String[] seqs = contents[1].split("SV=[0-9]+");
					if (seqs.length > 0) {
						this.proteinSequence = seqs[1];
						this.is_protein = true;
						this.aminoAcidLength = seqs[1].length();
					}
					//System.out.println(this.proteinSequence);
				}


				// sampleTitle
				if (contents[0].equals(GlycoSampleTerm.SAMPLE_TITLE.getTerm())) {
					this.sampleTitle = contents[1];
					//System.out.println(this.sampleTitle);
				}
				// sampleDescription
				if (contents[0].equals(GlycoSampleTerm.DESCRIPTION.getTerm())) {
					this.sampleDescription = contents[1];
				}
			}
		}


		// check objects
		/*
		System.out.println("print key and value for glycosamples");
		for (SampleKeyValue kv: this.keyvalueData){
			kv.print();
		}
		System.out.println("local id->"+ this.localId);
		System.out.println("date->"+ this.date);
		System.out.println("taxId->"+ this.taxId);
		*/




	}

	public void printText(){
		for (SampleKeyValue kv: this.keyvalueData){
			kv.print();
		}
	}



	public DiseaseSample(
			String a_taxId
			,String a_cellLien
			,String a_doId
			,String a_mesh
			,String a_disease
			,String a_tissue
			,String a_createDate
			,LinkedList<UniProt> a_uniprotId
			,String a_proteinName
			,String a_sampleTitle
			,String a_sampleDescription
			,SampleKeyValue o_kv
			){
		this.taxId = a_taxId;
		this.cellLien = a_cellLien;
		this.doId = a_doId;
		this.doId = a_mesh;
		this.disease = a_disease;
		this.tissue = a_tissue;
		this.createDate = a_createDate;
		this.uniprotId = a_uniprotId;
		this.proteinName = a_proteinName;
		this.sampleTitle = a_sampleTitle;
		this.sampleDescription = a_sampleDescription;
		this.keyvalueData.add(o_kv);
	}

	public String get_glycosampleId(){
		return this.glycosampleId;
	}


	public Integer get_aminoAcidLength() {
		return this.aminoAcidLength;
	}

	public Boolean isProtein() {
		return this.is_protein;
	}

	public String getlocalId(){
		return this.localId;
	}

	// taxId
	public void setTaxId(String a_taxId){
		this.taxId = a_taxId;
	}
	public String getTaxId(){
		return this.taxId;
	}

	// cellLien
	public void set_cellLien(String a_cellLien){
		this.cellLien = a_cellLien;
	}
	public String get_cellLien(){
		return this.cellLien;
	}

	// doId
	public void set_doId(String a_doId){
		this.doId = a_doId;
	}
	public String get_doId(){
		return this.doId;
	}

	// mesh
	public void set_mesh(String a_mesh){
		this.mesh = a_mesh;
	}
	public String get_mesh(){
		return this.mesh;
	}

	// desease
	public void set_disease(String a_disease){
		this.disease = a_disease;
	}
	public String get_disease(){
		return this.disease;
	}

	// tissue
	public void set_tissue(String a_tissue){
		this.tissue = a_tissue;
	}
	public String get_tissue(){
		return this.tissue;
	}

	// createDate
	public void set_createDate(String a_createDate){
		this.createDate = a_createDate;
	}
	public String get_createDate(){
		return this.createDate;
	}

	// uniprotId
	public void set_uniprotId(LinkedList<UniProt> a_uniprotId){
		this.uniprotId = a_uniprotId;
	}
	public LinkedList<UniProt> get_uniprot(){
		return this.uniprotId;
	}

	// proteinName
	public void set_proteinName(String a_proteinName){
		this.proteinName = a_proteinName;
	}
	public String get_proteinName(){
		return this.proteinName;
	}

	public String get_proteinSequence(){
		//System.out.println(this.proteinSequence);
		return this.proteinSequence;
	}

	// sampleTitle
	public void set_sampleTitle(String a_sampleTitle){
		this.sampleTitle = a_sampleTitle;
	}
	public String get_sampleTitle(){
		return this.sampleTitle;
	}

	// sampleDescription
	public void set_sampleDescription(String a_sampleDescription){
		this.sampleDescription = a_sampleDescription;
	}
	public String get_sampleDescription(){
		return this.sampleDescription;
	}

	// sampleDescription
	public void set_c(SampleKeyValue o_kv){
		this.keyvalueData.add(o_kv);
	}

	public LinkedList<SampleKeyValue> get_keyvalueData() {
		return this.keyvalueData;
	}

	/**
	 * @return health_state
	 */
	public String getHealth_state() {
		return health_state;
	}

	/**
	 * @param health_state セットする health_state
	 */
	public void setHealth_state(String health_state) {
		this.health_state = health_state;
	}

	/**
	 * @return is_lipid
	 */
	public Boolean getIs_lipid() {
		return is_lipid;
	}

	/**
	 * @param is_lipid セットする is_lipid
	 */
	public void setIs_lipid(Boolean is_lipid) {
		this.is_lipid = is_lipid;
	}





}
