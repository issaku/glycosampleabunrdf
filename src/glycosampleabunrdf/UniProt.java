package glycosampleabunrdf;

import java.util.LinkedList;

public class UniProt {

	private LinkedList<String> ids = new LinkedList<String>();
	private Boolean bl_part = false;
	private Boolean bl_mod = false;
	static final String PART_CHECK_FLAG = "#";
	static final String MOD_CHECK_FLAG = "\\?";
	static final String PART_SEPARATE = ",";

	public LinkedList<String> getUniProtIds() {
		return this.ids;
	}

	public void setUniProt(String a_UniProt_string) {

			String[] ar_ids = a_UniProt_string.split(PART_SEPARATE);
			for (String id : ar_ids) {
				// id #?P00120125
				if (a_UniProt_string.contains(PART_CHECK_FLAG)) {
					this.setBl_part(true);
				}
				if (a_UniProt_string.contains(MOD_CHECK_FLAG)) {
					this.setBl_mod(true);
				}
				this.ids.add(id.replaceAll(PART_CHECK_FLAG, "").replaceAll(MOD_CHECK_FLAG, "").trim());
			}
	}

	public void clear(){
		this.ids.clear();
	}

	/**
	 * @return bl_part
	 */
	public Boolean getBl_part() {
		return bl_part;
	}

	/**
	 * @param bl_part セットする bl_part
	 */
	public void setBl_part(Boolean bl_part) {
		this.bl_part = bl_part;
	}

	/**
	 * @return bl_mod
	 */
	public Boolean getBl_mod() {
		return bl_mod;
	}

	/**
	 * @param bl_mod セットする bl_mod
	 */
	public void setBl_mod(Boolean bl_mod) {
		this.bl_mod = bl_mod;
	}




}
