package glycosampleabunrdf;

import java.util.LinkedList;

public class DbMemoItem {
	private String db_localIds = "";
	private String db_Edited = "";
	private String db_Original = "";

	public DbMemoItem(String a_db_localIds
			,String a_db_Edited
			,String a_db_Original
			) {
		this.db_localIds = a_db_localIds;
		this.db_Edited = a_db_Edited;
		this.db_Original = a_db_Original;
	}

	public String[] get_db_localId_List() {

		String[] list = this.db_localIds.replaceAll(" ", "").split(",");
		return list;
	}

	public String get_db_localIds() {
		return this.db_localIds;
	}

	public String get_db_Edited() {
		return this.db_Edited;
	}

	public String get_db_Original() {
		return this.db_Original;
	}


}
