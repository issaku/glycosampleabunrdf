package glycosampleabunrdf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Abundance {

	/*
	abunID	positionType	position	GlycanType	memo1
	abunID	abundanceType	abundanceRatio	Glycan	memo2
	2-0 = 1	position(aa)	N184	GlyTouCan	Text(p2308)
	2-0 = 1	%	68.0	G22340YC	Text(p2308)
	 */

	private String posID = "";
	private String positionType = "";
	private String position = "";
	private String int_position = "";
	private String attached_position_AA = "";
	private String glycanType = "";
	private String memoPos = "";

	private String ratioID = "";
	private String ratioType = "";
	private String ratio = "";
	private String glycan = "";
	private String memoRatio = "";

	private String local_glycan_abun_pos = "";
	private String local_glycan = "";


	public String getPosID() {
		return this.posID;
	}
	public String getPositionType() {
		return this.positionType;
	}
	public String getPosition() {
		return this.position;
	}
	public String getIntPosition() {
		return this.int_position;
	}
	public String getAttached_position_AA() {
		return this.attached_position_AA;
	}
	public String getGlycanType() {
		return this.glycanType;
	}
	public String getMemoPos() {
		return this.memoPos;
	}

	public String getRatioID() {
		return this.ratioID;
	}
	public String getAbundanceType() {
		return this.ratioType;
	}
	public String getAbundanceRatio() {
		return this.ratio;
	}
	public String getGlycan() {
		return this.glycan;
	}
	public String getMemoRatio() {
		return this.memoRatio;
	}

/*
 									* tmp_posID
									, tmp_positionType
									, tmp_position
									, tmp_glycanType
									, tmp_memoPos
									, tmp_ratioID
									, tmp_ratioType
									, tmp_ratio
									, tmp_glycan
									, tmp_local_glycan_abun_pos
									, tmp_local_glycan
									, tmp_memoRatio
 */
	public void setAbundance(
			String a_abunPosID
			, String a_positionType
			, String a_position
			, String a_glycanType
			, String a_memoPos
			, String a_abunRatioID
			, String a_abundanceType
			, String a_abundanceRatio
			, String a_glycan
			, String a_local_glycan_abun_pos
			, String a_local_glycan
			, String a_memoRatio) {

		this.posID = a_abunPosID;
		this.positionType = a_positionType;
		this.position = a_position;
		this.glycanType = a_glycanType;
		this.memoPos = a_memoPos;

/*
		System.out.println(this.posID);
		System.out.println(this.positionType);
		System.out.println(this.position);
		System.out.println(this.glycanType);
		System.out.println(this.memoPos);
*/

        String tmp_attached_position_AA = "";
        String tmp_int_position = "";
        try {
	        Pattern pattern = Pattern.compile("(?<alphabets>[a-zA-Z]+)(?<numbers>[0-9]+)");
	        Matcher matcher = pattern.matcher(a_position);
	        while (matcher.find()) {
	            //System.out.println("==========");
	            //System.out.println("group(alphabets)=" + matcher.group("alphabets"));
	            //System.out.println("group(numbers)=" + matcher.group("numbers"));
	        	tmp_attached_position_AA = matcher.group("alphabets");
	        	tmp_int_position = matcher.group("numbers");
	        }
        }
        catch (Exception e) {

        }
        this.attached_position_AA = tmp_attached_position_AA;
        this.int_position = tmp_int_position;

		this.ratioID = a_abunRatioID;
		this.ratioType = a_abundanceType;
		this.ratio = a_abundanceRatio;
		this.glycan = a_glycan;
		this.local_glycan_abun_pos = a_local_glycan_abun_pos;
		this.local_glycan = a_local_glycan;
		this.memoRatio = a_memoRatio;
	}

	public void printText() {

		System.out.println("PosID\t"+ this.posID);
		System.out.println("PositionType\t"+ this.positionType);
		System.out.println("Position\t"+ this.position);
		System.out.println("IntPosition\t"+ this.int_position);
		System.out.println("Attached_position_AA\t"+ this.attached_position_AA);
		System.out.println("GlycanType\t"+ this.glycanType);
		System.out.println("local_glycan_abun_pos\t"+ this.local_glycan_abun_pos);
		System.out.println("MemoPos\t"+ this.memoPos);

		System.out.println("RatioID\t"+ this.ratioID);
		System.out.println("AbundanceType\t"+ this.ratioType);
		System.out.println("AbundanceRatio\t"+ this.ratio);
		System.out.println("Glycan\t"+ this.glycan);
		System.out.println("local_glycan\t"+ this.local_glycan);
		System.out.println("MemoRatio\t"+ this.memoRatio);

	}
	/**
	 * @return local_glycan
	 */
	public String getLocal_glycan() {
		return local_glycan;
	}
	/**
	 * @param local_glycan セットする local_glycan
	 */
	public void setLocal_glycan(String local_glycan) {
		this.local_glycan = local_glycan;
	}
	/**
	 * @return local_glycan_abun_pos
	 */
	public String getLocal_glycan_abun_pos() {
		return local_glycan_abun_pos;
	}
	/**
	 * @param local_glycan_abun_pos セットする local_glycan_abun_pos
	 */
	public void setLocal_glycan_abun_pos(String local_glycan_abun_pos) {
		this.local_glycan_abun_pos = local_glycan_abun_pos;
	}



}
