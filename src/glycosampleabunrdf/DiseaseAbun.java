package glycosampleabunrdf;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DiseaseAbun {

	private LinkedList<LinkedList<Abundance>> a_set = new LinkedList<LinkedList<Abundance>>();
	private LinkedList<Abundance> a_abuns = new LinkedList<Abundance>();
	private LinkedList<Memo> a_memos = new LinkedList<Memo>();

	public LinkedList<LinkedList<Abundance>> getSet() {
		return this.a_set;
	}

	//public LinkedList<Abundance> getAbundanceList() {
	//	return this.a_abuns;
	//}

	public LinkedList<Memo> getMemos(){
		return this.a_memos;
	}

	public DiseaseAbun(String str_GlycoAbunData) {
		// separate to lines
		String[] lines = str_GlycoAbunData.split("\n");
		// check tab, tab"{data}"tab

		// remove \n and \" => datalines
		boolean bl_double_quotation = false;
		String str_dataline = "";
		LinkedList<String> datalines = new LinkedList<String>();
		String str_rn = "\r\n";
		String str_n = "\n";
		String str_nr = "\n\r";

		for (int i=0; i<lines.length; ++i) {
			//str_dataline = lines[i];

			// check of count "\""
			int count_double_quotation = 0;
			String[] split_double_quotation = lines[i].split("\"");
			// "\"" = 1 => count_double_quotation = 1; "\"" = 2 => count_double_quotation = 2
			count_double_quotation = split_double_quotation.length - 1;

			if (count_double_quotation%2 != 0) {

				str_dataline += lines[i].replaceAll("\"", "").replaceAll(str_rn, " ").replaceAll(str_nr, " ").replaceAll(str_n, " ");

				if (bl_double_quotation == false) {
					if (count_double_quotation%2 != 0) {
						bl_double_quotation = true;
					}
					else if (count_double_quotation%2 == 0) {
						bl_double_quotation = false;
					}
				}
				else {
					datalines.add(str_dataline);
					str_dataline = "";
					//bl_double_quotation = false;
					if (count_double_quotation%2 != 0) {
						bl_double_quotation = false;
					}
					else if (count_double_quotation%2 == 0) {
						bl_double_quotation = true;
					}
				}
			}
			else {

				if (bl_double_quotation == false) {
					str_dataline += lines[i].replaceAll("\"", "").replaceAll(str_rn, " ").replaceAll(str_nr, " ").replaceAll(str_n, " ");
					datalines.add(str_dataline);
					str_dataline = "";
				}
				else {
					str_dataline += lines[i].replaceAll("\"", "").replaceAll(str_rn, " ").replaceAll(str_nr, " ").replaceAll(str_n, " ");
					//if (str_dataline.contains("Memo")) {
					//	str_dataline += lines[i].replaceAll("\n", " ");
					//}
					//else {
					//	str_dataline += lines[i] + " ";
					//}
				}
			}
		}


		//LinkedList<Memo> a_memos = new LinkedList<Memo>();
		//Abundance abun = new Abundance();
		//LinkedList<Abundance> a_abuns = new LinkedList<Abundance>();

		String tmp_posID = "";
		String tmp_positionType = "";
		String tmp_position = "";
		String tmp_glycanType = "";
		String tmp_memoPos = "";

		String tmp_ratioID = "";
		String tmp_ratioType = "";
		String tmp_ratio = "";
		String tmp_glycan = "";
		String tmp_local_glycan_abun_pos = "";
		String tmp_local_glycan = "";
		String tmp_memoRatio = "";

		for (String line: datalines){
			//System.out.println(line);
			String[] contents = line.split("\t");
			if (contents.length > 0) {

				// Memo
				if (contents[0].contains("Memo")) {
					//DB Memo
					//Discussion Memo
					if (contents.length > 3) {
						if (contents[0].contains("DB Memo")
								&&!contents[1].contains("Edited")
								&& !contents[2].contains("Original")
								&& !contents[3].contains("local")) {
							// DB Memo	Edited Memo	Original Memo	local ID

							Memo mm = new Memo();
							mm.setDbMemoItem(contents[1], contents[2], contents[3]);
							a_memos.add(mm);
						}
						//Discussion Memo	Edited Memo	Original Memo	local ID
						if (contents[0].contains("Discussion Memo")
								&&!contents[1].contains("Edited")
								&& !contents[2].contains("Original")
								&& !contents[3].contains("local")) {
							// DB Memo	Edited Memo	Original Memo	local ID

							Memo mm = new Memo();
							mm.setDiscussionMemo(contents[1], contents[2], contents[3]);
							a_memos.add(mm);
						}

					}
					if (contents.length > 1) {
						if (contents[0].contains("Internal Memo")) {
						// Internal Memo
							Memo mm = new Memo();
							String t_memo = "";
							for (String t_mm : contents) {
								t_memo += t_mm + "\t";
							}
							mm.setInternalMemo(t_memo);
							this.a_memos.add(mm);
						}
					}


				}
				// abundance ratio
				else {
					// local_id	pos/%			pos/%	glycan
					// 2-0 = 1	position(aa)	N184	GlyTouCan	Text(p2308)
					// 2-0 = 1	%				68.0	G22340YC	Text(p2308)
					String[] abs = line.split("\t");
					if (abs.length > 5) {

						// position
						if (abs[1].contains("position(aa)")) {
							tmp_posID = abs[0];
							tmp_positionType = abs[1];
							tmp_position = abs[2];
							tmp_glycanType = abs[3];
							tmp_local_glycan_abun_pos = abs[4];
							tmp_memoPos = abs[5];
							//abun.setAbunPos(abs[0], abs[1], abs[2], abs[3], abs[4]);
							//System.out.println("abundance\t"+ abs[0] + "\t"+ abs[1] + "\t"+ abs[2] + "\t"+ abs[3] + "\t"+ abs[4] + "\t");
						}
						// % or mol
						if (abs[1].contains("%") || abs[1].contains("mol")) {
							tmp_ratioID = abs[0];
							tmp_ratioType = abs[1];
							tmp_ratio = abs[2];
							tmp_glycan = abs[3];
							tmp_local_glycan = abs[4];
							tmp_memoRatio = abs[5];

							Abundance abun = new Abundance();
							abun.setAbundance(
									tmp_posID
									, tmp_positionType
									, tmp_position
									, tmp_glycanType
									, tmp_memoPos
									, tmp_ratioID
									, tmp_ratioType
									, tmp_ratio
									, tmp_glycan
									, tmp_local_glycan_abun_pos
									, tmp_local_glycan
									, tmp_memoRatio);
							//System.out.println("ratio\t"+ abs[0] + "\t"+ abs[1] + "\t"+ abs[2] + "\t"+ abs[3] + "\t"+ abs[4] + "\t");
							this.a_abuns.add(abun);
						}
					}

				}
			}
		}

		// separate Set items
		LinkedList<Abundance> t_abuns = new LinkedList<Abundance>();
		LinkedList<String> poss = new LinkedList<String>();
		//System.out.println("---------------------");
		for (Abundance ab: this.a_abuns){
			String pos = ab.getPosition();
			poss.add(pos);
			String gly = ab.getGlycan();
			poss.add(gly);
			//System.out.println(pos + "\t" + gly);
		}

		//System.out.println("---------------------");
		for (String p : poss){
			//System.out.println(p);
		}

		// delete duplicate positions
		List<String> uniq_poss = poss.stream().distinct().collect(Collectors.toList());

		//System.out.println("---------------------");
		for (String p : uniq_poss){
			//System.out.println(p);
		}

		for (int i=0; i<uniq_poss.size(); ++i) {
			String up = uniq_poss.get(i);
			for (Abundance ab: this.a_abuns){
				String pos = ab.getPosition();
				if (up.equals(pos)) {
					t_abuns.add(ab);
				}
				else {
					if (t_abuns.size() > 0) {
						this.a_set.add(t_abuns);
					}
					t_abuns = new LinkedList<Abundance>();
					//continue;
				}
			}
		}

		this.a_set.removeAll(Collections.singleton(null));

		//System.out.println("------- Set ----------");
		for (LinkedList<Abundance> t_ab: this.a_set){
			//System.out.println("--- Position ----------");
			for (Abundance ab: t_ab){
				String pos = ab.getPosition();
				String gly = ab.getGlycan();
				//System.out.println(pos + "\t" + gly);
			}
		}



		// check abundance ratio
		/*
		getAbunPosID
		getPositionType
		getPosition
		getIntPosition
		getAttached_position_AA
		getGlycanType
		getMemoPos
		*/
		for (Abundance ab: a_abuns){
			/*
			System.out.println("---------------------");
			System.out.println("PosID\t"+ ab.getPosID());
			System.out.println("PositionType\t"+ ab.getPositionType());
			System.out.println("Position\t"+ ab.getPosition());
			System.out.println("IntPosition\t"+ ab.getIntPosition());
			System.out.println("Attached_position_AA\t"+ ab.getAttached_position_AA());
			System.out.println("GlycanType\t"+ ab.getGlycanType());
			System.out.println("MemoPos\t"+ ab.getMemoPos());

			System.out.println("RatioID\t"+ ab.getRatioID());
			System.out.println("AbundanceType\t"+ ab.getAbundanceType());
			System.out.println("AbundanceRatio\t"+ ab.getAbundanceRatio());
			System.out.println("Glycan\t"+ ab.getGlycan());
			System.out.println("MemoRatio\t"+ ab.getMemoRatio());
			*/
		}


		// check Memos
/*
		for (Memo mm: a_memos){
			// DB Memo
			for (String[] ids: mm.getDbMemo_localIds()) {
				for (String id: ids) {
					System.out.println("DbMemo_localIds->"+ id);
				}
			}
			for (String DbMemo_Edited: mm.getDbMemo_Edited()) {
				System.out.println("DbMemo_Edited->"+ DbMemo_Edited);
			}
			for (String DbMemo_Original: mm.getDbMemo_Original()) {
				System.out.println("DbMemo_Original->"+ DbMemo_Original);
			}
			// Discussion Memo
			for (String[] ids: mm.getDiscussionMemo_localIds()) {
				for (String id: ids) {
					System.out.println("DiscussionMemo_localIds->"+ id);
				}
			}
			for (String DiscussionMemo_Edited: mm.getdDiscussionMemo_Edited()) {
				System.out.println("DiscussionMemo_Edited->"+ DiscussionMemo_Edited);
			}
			for (String DiscussionMemo_Original: mm.getDiscussionMemo_Original()) {
				System.out.println("DiscussionMemo_Original->"+ DiscussionMemo_Original);
			}
			// InternalMemo
			for (String InternalMemo: mm.getInternalMemo()) {
				System.out.println("InternalMemo->"+ InternalMemo);
			}
		}
*/
	}
}
