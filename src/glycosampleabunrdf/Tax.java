package glycosampleabunrdf;

public enum Tax {

	HUMAN         ("9606",                 "Homo sapiens");



	private String id;
	private String organism;

	public String getId() {
		return this.id;
	}

	public String getOrganism() {
		return this.organism;
	}

	Tax(String a_id, String a_organism){
		this.id = a_id;
		this.organism = a_organism;
	}

}