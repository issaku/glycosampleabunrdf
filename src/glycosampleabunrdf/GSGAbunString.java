package glycosampleabunrdf;


public class GSGAbunString {

	private String sourceData = "";
	private String sampleData = "";
	private String abunData = "";
//	private String memoData = "";

	public GSGAbunString(String a_sourceData
			, String a_sampleData
			, String a_abunData) { //, String a_memoData){
		this.sourceData = a_sourceData;
		this.sampleData = a_sampleData;
		this.abunData = a_abunData;
		//this.memoData = a_memoData;
	}

	public String getSourceData(){
		return this.sourceData;
	}
	public String getSampleData(){
		return this.sampleData;
	}
	public String getAbunData(){
		return this.abunData;
	}
	/*
	public String getMemoData(){
		return this.memoData;
	}
	*/

}
