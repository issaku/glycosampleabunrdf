package glycosampleabunrdf;

import java.util.LinkedList;

public class SetList {

	LinkedList<Set> setlist = new LinkedList<Set>();

	public SetList(Set a_set) {
		this.setlist.add(a_set);
	}

	public LinkedList<Set> getSetList() {
		return this.setlist;
	}

	public void setSetList(Set a_set) {
		this.setlist.add(a_set);
	}

	public void clear(){
		this.setlist.clear();
	}

}
