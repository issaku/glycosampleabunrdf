package glycosampleabunrdf;

import java.util.LinkedList;

public class Memo {

	// Edited Memo, Original Memo, local ID
	private String dbMemo_localIds_string = "";
	private LinkedList<String[]> dbMemo_localIds = new LinkedList<String[]>();
	private LinkedList<String> dbMemo_Edited = new LinkedList<String>();
	private LinkedList<String> dbMemo_Original = new LinkedList<String>();
	private LinkedList<String[]> discussionMemo_localIds = new LinkedList<String[]>();
	private LinkedList<String> discussionMemo_Edited = new LinkedList<String>();
	private LinkedList<String> discussionMemo_Original = new LinkedList<String>();
	private LinkedList<String> internalMemo = new LinkedList<String>();
	private LinkedList<String> DbMemo_localIds_List = new LinkedList<String>();
	private LinkedList<DbMemoItem> DbMemoItems = new LinkedList<DbMemoItem>();


	public String get_dbMemo_localIds_string() {
		String t_out = "";
		for (String[] ar : this.dbMemo_localIds) {
			for (String str : ar) {
				t_out += str + "_";
			}
		}
		return t_out;
	}

	public LinkedList<DbMemoItem> get_DbMemoItems(){
		return this.DbMemoItems;
	}

	public LinkedList<String> getDbMemo_localIds_List(){
		return this.DbMemo_localIds_List;
	}

	public LinkedList<String[]> getDbMemo_localIds(){
		return this.dbMemo_localIds;
	}
	public LinkedList<String> getDbMemo_Edited(){
		return this.dbMemo_Edited;
	}
	public LinkedList<String> getDbMemo_Original(){
		return this.dbMemo_Original;
	}

	public LinkedList<String[]> getDiscussionMemo_localIds(){
		return this.discussionMemo_localIds;
	}
	public LinkedList<String> getdDiscussionMemo_Edited(){
		return this.discussionMemo_Edited;
	}
	public LinkedList<String> getDiscussionMemo_Original(){
		return this.discussionMemo_Original;
	}

	public LinkedList<String> getInternalMemo(){
		return this.internalMemo;
	}


	public void setDbMemoItem(String a_memo_Edited
			, String a_memo_Original
			, String a_localIds) {
		DbMemoItem dbmi = new DbMemoItem(
				a_localIds
				, a_memo_Edited
				, a_memo_Original
				);
		DbMemoItems.add(dbmi);

		this.dbMemo_Edited.add(a_memo_Edited);
		this.dbMemo_Original.add(a_memo_Original);
		String[] ids = a_localIds.replaceAll(" ", "").split(",");
		this.dbMemo_localIds.add(ids);

		String t_str = a_localIds.replaceAll(" ", "").replaceAll(",", "_");
		this.DbMemo_localIds_List.add(t_str);
	}


	/*
	Edited Memo	Original Memo	local IDs
	 */
	public void setDbMemo_dame(String a_memo_Edited
			, String a_memo_Original
			, String a_localIds) {
		this.dbMemo_Edited.add(a_memo_Edited);
		this.dbMemo_Original.add(a_memo_Original);
		String[] ids = a_localIds.replaceAll(" ", "").split(",");
		this.dbMemo_localIds.add(ids);

		String t_str = a_localIds.replaceAll(" ", "").replaceAll(",", "_");
		this.DbMemo_localIds_List.add(t_str);
	}

	/*
	Edited Memo	Original Memo	local IDs
	 */
	public void setDiscussionMemo(String a_memo_Edited
			, String a_memo_Original
			, String a_localIds) {
		this.discussionMemo_Edited.add(a_memo_Edited);
		this.discussionMemo_Original.add(a_memo_Original);
		String[] ids = a_localIds.replaceAll(" ", "").split(",");
		this.discussionMemo_localIds.add(ids);
	}

	public void setInternalMemo(String a_memo) {
		this.internalMemo.add(a_memo);
	}

}
