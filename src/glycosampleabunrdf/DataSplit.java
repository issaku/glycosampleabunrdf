package glycosampleabunrdf;

import java.util.LinkedList;

public class DataSplit {

	private String str_SampleSource = "";
	private String str_SampleLines = "";
	private String str_AbundanceMemoLines = "";

		/*
		Source
			PaperID
			Source
		Sample Lines
			GlycoSampleID
		Abundance Lines
			Abundance
			Memo Lines
				DB Memo
				Discussion Memo
				Internal Memo 1
				Internal Memo 2
		 */


	public LinkedList<GSGAbunString> dataSplit(String o_strData) {
		LinkedList<GSGAbunString> str_Entities = new LinkedList<GSGAbunString>();
		/*
		 split "GlycoSampleID"
		 [0] Source
		 [1] others
		 */
		String[] o_GSGAbun = o_strData.split("GlycoSampleID", 0);

		if (o_GSGAbun.length > 0) {

			String[] ar_SampleDource = o_GSGAbun[0].split("\n");

			for (int i=0; i<ar_SampleDource.length; ++i) {

				String strline = util.removeTabReturn(ar_SampleDource[i]);
				//System.out.println(strline);
				if (strline.getBytes().length > 0) {
					if (ar_SampleDource[i].contains("\n")) {
						this.str_SampleSource += ar_SampleDource[i];
					}
					else {
						this.str_SampleSource += ar_SampleDource[i] + "\n";
						//this.str_SampleSource += ar_SampleDource[i];
					}
				}
			}

			//System.out.println(this.str_SampleSource);

			if (o_GSGAbun.length > 1) {

				for (int i=1; i<o_GSGAbun.length; ++i) {
					/*
					split "Abundance" [1] others
					[0] Sample Lines
					[1] Abundance & Memo Lines
					 */
					//String[] o_samabun = o_GSGAbun[i].split("Abundance", 0);
					String[] o_samabun = o_GSGAbun[i].split("Abundance");

					// get sample lines
					if (o_samabun.length > 0) {
						this.str_SampleLines = o_samabun[0];
						//System.out.println(o_samabun[0]);
					}
					// get abundance and memo lines
					if (o_samabun.length > 1) {
						this.str_AbundanceMemoLines = o_samabun[1];
						//System.out.println(o_samabun[1]);
					}
					// set lines to LinkedList<GSGAbun>
					GSGAbunString o_gs = new GSGAbunString(this.str_SampleSource
							, this.str_SampleLines
							, this.str_AbundanceMemoLines);
					str_Entities.add(o_gs);
				}
			}
		}
		return str_Entities;
	}

}
