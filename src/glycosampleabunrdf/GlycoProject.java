package glycosampleabunrdf;

import java.util.Arrays;

public class GlycoProject {

	private String localId = "";
	private String glycoProjectId = "";
	private String project_title = "";
	private String project_description = "";
	private String glycoProjectIdString = "";
	private String[] glycoProjectIds;


	public GlycoProject(String str_GlycoProjectDataLine) {
			String[] prodata = str_GlycoProjectDataLine.split("\t");
			if (prodata.length > 0) {
				this.glycoProjectId = prodata[0];
			}
			if (prodata.length > 1) {
				this.localId = prodata[1];
			}
			if (prodata.length > 2) {
				String[] t_data = prodata[2].replaceAll(" ", ",").replaceAll(",,", ",").replaceAll(", ", ",").split(",");

				Arrays.sort(t_data);
				String t_ids = "";
				for (String str : t_data) {
					t_ids += str + "_";
				}
				this.glycoProjectIdString = t_ids;

				this.glycoProjectIds = t_data;
			}
			if (prodata.length > 3) {
				String t_data = prodata[3];
				this.project_title = t_data;
			}
			if (prodata.length > 4) {
				String t_data = prodata[4];
				this.project_description = t_data;
			}


	}

	public GlycoProject(
			String a_localId
			,String a_glycoProjectId
			,String a_glycoSampleIdString
			,String[] a_glycoSamples
			,String a_glycoProjectIdString
			,String[] a_glycoProjects
			, String a_project_title
			, String a_project_description
			){
		this.localId = a_localId;
		this.glycoProjectId = a_glycoProjectId;
		this.glycoProjectIdString = a_glycoProjectIdString;
		this.glycoProjectIds = a_glycoProjects;
		this.project_title = a_project_title;
		this.project_description = a_project_description;
	}


	// glycoProjectId
	public String get_glycoProjectId(){
		return this.glycoProjectId;
	}

	// localId
	public String get_localId(){
		return this.localId;
	}

	/**
	 * @return project_title
	 */
	public String getProject_title() {
		return project_title;
	}

	/**
	 * @param project_title セットする project_title
	 */
	public void setProject_title(String project_title) {
		this.project_title = project_title;
	}

	/**
	 * @return glycoProjectIds
	 */
	public String[] getGlycoProjectIds() {
		return glycoProjectIds;
	}

	/**
	 * @param glycoProjectIds セットする glycoProjectIds
	 */
	public void setGlycoProjectIds(String[] glycoProjectIds) {
		this.glycoProjectIds = glycoProjectIds;
	}

	/**
	 * @return glycoProjectIdString
	 */
	public String getGlycoProjectIdString() {
		return glycoProjectIdString;
	}

	/**
	 * @param glycoProjectIdString セットする glycoProjectIdString
	 */
	public void setGlycoProjectIdString(String glycoProjectIdString) {
		this.glycoProjectIdString = glycoProjectIdString;
	}

	/**
	 * @return project_description
	 */
	public String getProject_description() {
		return project_description;
	}

	/**
	 * @param project_description セットする project_description
	 */
	public void setProject_description(String project_description) {
		project_description = project_description;
	}





}
