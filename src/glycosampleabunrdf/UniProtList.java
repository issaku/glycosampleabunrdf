package glycosampleabunrdf;

import java.util.LinkedList;

public class UniProtList {

	private LinkedList<UniProt> uniProt_Data = new LinkedList<UniProt>();
	//static final String PART_CHECK_FLAG = "#";
	//static final String MOD_CHECK_FLAG = "?";
	static final String PART_SEPARATE = "&";
	/**
	 * @return uniProt_Data
	 */
	public LinkedList<UniProt> getUniProt_Data() {
		return uniProt_Data;
	}
	/**
	 * @param a_uniProt_Data セットする uniProt_Data
	 */
	public void setUniProt_Data(LinkedList<UniProt> a_uniProt_Data) {
		uniProt_Data = a_uniProt_Data;
	}


	public UniProtList(String a_uniprot_string) {
/*
		if (a_uniprot_string.contains(PART_CHECK_FLAG)) {
			String[] ar_ids = a_uniprot_string.substring(1).split(PART_SEPARATE);
			for (String id : ar_ids) {
				UniProt up = new UniProt();
				up.setUniProt(PART_CHECK_FLAG + id);
				this.UniProts.add(up);
			}
		}
		else if (!a_uniprot_string.contains(PART_CHECK_FLAG)) {
			String[] ar_ids = a_uniprot_string.split(PART_SEPARATE);
			for (String id : ar_ids) {
				UniProt up = new UniProt();
				up.setUniProt(id);
				this.UniProts.add(up);
			}
		}
*/
		// PART_SEPARATE = &

		String[] ar_ids = a_uniprot_string.split(PART_SEPARATE);
		for (String id : ar_ids) {
			// id contains #, ?
			UniProt up = new UniProt();
			up.setUniProt(id);
			this.uniProt_Data.add(up);
		}

	}





}
