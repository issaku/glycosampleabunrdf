package glycosampleabunrdf;

public enum PredicateTerms {

	// GlycoCoO	http://purl.jp/bio/12/glyco/conjugate#
	amino_acid_length    ("http://purl.jp/bio/12/glyco/conjugate#amino_acid_length"),
	has_Protein    ("http://purl.jp/bio/12/glyco/conjugate#has_protein"),
	has_Protein_Part    ("http://purl.jp/bio/12/glyco/conjugate#has_protein_part"),
	has_Saccharide_Part    ("http://purl.jp/bio/12/glyco/conjugate#has_saccharide_part"),
	has_Saccharide_Set  ("http://purl.jp/bio/12/glyco/conjugate#has_saccharide_set"),
	has_Attached_Referenced_Saccharide  ("http://purl.jp/bio/12/glyco/conjugate#has_attached_referenced_saccharide"),
	relative_abundance_percentage  ("http://purl.jp/bio/12/glyco/conjugate#relative_abundance_percentage"),
	glycosylated_at  ("http://purl.jp/bio/12/glyco/conjugate#glycosylated_at"),

	// SIO
	has_SetItem         ("http://semanticscience.org/resource/SIO_000313"),  // has-setitem
	refers_to         ("http://semanticscience.org/resource/SIO_000628"),  // refers-to
	has_Value         ("http://semanticscience.org/resource/SIO_000300"),  // has_value
	has_attribute         ("http://semanticscience.org/resource/SIO_000008"),  // has-attribute

	// dcterms:references
	//dcterms: <http://purl.org/dc/terms/> .
	// <a resource> dcterms:references pubmed:24495517 .
	references        ("http://purl.org/dc/terms/references"),  // dcterms:references


	// Skos
	exactMatch         ("http://www.w3.org/2004/02/skos/core#exactMatch"),  // refers-to
	altLabel         ("http://www.w3.org/2004/02/skos/core#altLabel"),  // skos:altLabel
	note         ("http://www.w3.org/2004/02/skos/core#note"),  // skos:altLabel

	// DC
	title         ("http://purl.org/dc/elements/1.1/title"),

	// Foaf
	//primaryTopic
	primaryTopic        ("http://xmlns.com/foaf/0.1/primaryTopic"),


	// FALDO
	location       ("http://biohackathon.org/resource/faldo#location"),
	position       ("http://biohackathon.org/resource/faldo#position"),

	// GlycoRDF
	has_association      ("http://purl.jp/bio/12/glyco/glycan#has_association"),
	is_from_source      ("http://purl.jp/bio/12/glyco/glycan#is_from_source"),
	has_tissue      ("http://purl.jp/bio/12/glyco/glycan#has_tissue"),
	has_cell_line      ("http://purl.jp/bio/12/glyco/glycan#has_cell_line"),
	has_taxon      ("http://purl.jp/bio/12/glyco/glycan#has_taxon"),
	published_in      ("http://purl.jp/bio/12/glyco/glycan#published_in"),
	has_Glycan         ("http://purl.jp/bio/12/glyco/glycan#has_glycan"),
	has_amino_acid         ("http://purl.jp/bio/12/glyco/glycan#has_amino_acid"),

	// GlycoNAVI
	has_Ref_Comp         ("http://purl.jp/bio/12/glyco/glyconavi#has_reference_compound"),
	is_Glycan         ("http://purl.jp/bio/12/glyco/glyconavi#is_glycan"),
	is_GlycanType         ("http://purl.jp/bio/12/glyco/glyconavi#is_glycan_type"),
	has_GlycanType         ("http://purl.jp/bio/12/glyco/glyconavi#has_glycan_type"),
	disease         ("http://purl.jp/bio/12/glyco/glyconavi#disease"),
	has_doid         ("http://purl.jp/bio/12/glyco/glyconavi#has_doid"),
	has_mesh         ("http://purl.jp/bio/12/glyco/glyconavi#has_mesh"),
	has_Health_state         ("http://purl.jp/bio/12/glyco/glyconavi#has_Health_state"),

	is_part         ("http://purl.jp/bio/12/glyco/glyconavi#is_part"),
	has_mod         ("http://purl.jp/bio/12/glyco/glyconavi#has_mod"),

	has_Abundance         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoSample#has_abundance"),
	organism         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoSample#organism"),
	taxonomy         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoSample#taxonomy"),
	gene         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoSample#gene"),

	has_Sample         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoAbun#has_sample"),
	has_DB_Memo         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoProject#has_db_memo"),

	id         ("http://purl.jp/bio/12/glyco/glyconavi#id"),
	AA_Seq         ("http://purl.jp/bio/12/glyco/glyconavi/GlycoSample#has_amino_acid__seq"),
	NCIT_C25447         ("http://purl.obolibrary.org/obo/NCIT_C25447"),
	PROPERTYTYPE        ("http://www.w3.org/1999/02/22-rdf-syntax-ns#propertyType"),
	PROPERTYVALUE        ("http://www.w3.org/1999/02/22-rdf-syntax-ns#propertyValue");



	private String predicate;

	public String getPredicate() {
		return this.predicate;
	}

	PredicateTerms(String a_predicate){
		this.predicate = a_predicate;
	}

}
