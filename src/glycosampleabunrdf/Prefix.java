package glycosampleabunrdf;

public class Prefix {

	private String str_GlycoSamplePrefix
				= "@prefix " + PrefixTerm.GCO.getPrefix() + ": <" +  PrefixTerm.GCO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.DCTERMS.getPrefix() + ": <" +  PrefixTerm.DCTERMS.getUri() + ">\n"
				//+ "@prefix " + PrefixTerm.DC.getPrefix() + ": <" +  PrefixTerm.DC.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.RDFS.getPrefix() + ": <" +  PrefixTerm.RDFS.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.RDF.getPrefix() + ": <" +  PrefixTerm.RDF.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.FALDO.getPrefix() + ": <" +  PrefixTerm.FALDO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.UP.getPrefix() + ": <" +  PrefixTerm.UP.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.SIO.getPrefix() + ": <" +  PrefixTerm.SIO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.OBO.getPrefix() + ": <" +  PrefixTerm.OBO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.EDAM.getPrefix() + ": <" +  PrefixTerm.EDAM.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.DS.getPrefix() + ": <" +  PrefixTerm.DS.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.GA.getPrefix() + ": <" +  PrefixTerm.GA.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.GLYCAN.getPrefix() + ": <" +  PrefixTerm.GLYCAN.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.GTCI.getPrefix() + ": <" +  PrefixTerm.GTCI.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.GTC.getPrefix() + ": <" +  PrefixTerm.GTC.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.FOAF.getPrefix() + ": <" +  PrefixTerm.FOAF.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.CODAO.getPrefix() + ": <" +  PrefixTerm.CODAO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.UO.getPrefix() + ": <" +  PrefixTerm.UO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.SKOS.getPrefix() + ": <" +  PrefixTerm.SKOS.getUri() + ">\n";


	public String getGlycoSamplePrefix() {
		return this.str_GlycoSamplePrefix;
	}

	private String str_GlycoAbunPrefix
			= "@prefix " + PrefixTerm.GCO.getPrefix() + ": <" +  PrefixTerm.GCO.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.DCTERMS.getPrefix() + ": <" +  PrefixTerm.DCTERMS.getUri() + ">\n"
			//+ "@prefix " + PrefixTerm.DC.getPrefix() + ": <" +  PrefixTerm.DC.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.RDFS.getPrefix() + ": <" +  PrefixTerm.RDFS.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.RDF.getPrefix() + ": <" +  PrefixTerm.RDF.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.FALDO.getPrefix() + ": <" +  PrefixTerm.FALDO.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.UP.getPrefix() + ": <" +  PrefixTerm.UP.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.SIO.getPrefix() + ": <" +  PrefixTerm.SIO.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.OBO.getPrefix() + ": <" +  PrefixTerm.OBO.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.EDAM.getPrefix() + ": <" +  PrefixTerm.EDAM.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.DS.getPrefix() + ": <" +  PrefixTerm.DS.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.GA.getPrefix() + ": <" +  PrefixTerm.GA.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.GLYCAN.getPrefix() + ": <" +  PrefixTerm.GLYCAN.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.GTCI.getPrefix() + ": <" +  PrefixTerm.GTCI.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.GTC.getPrefix() + ": <" +  PrefixTerm.GTC.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.FOAF.getPrefix() + ": <" +  PrefixTerm.FOAF.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.CODAO.getPrefix() + ": <" +  PrefixTerm.CODAO.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.UO.getPrefix() + ": <" +  PrefixTerm.UO.getUri() + ">\n"
			+ "@prefix " + PrefixTerm.SKOS.getPrefix() + ": <" +  PrefixTerm.SKOS.getUri() + ">\n";

	public String getGlycoAbunPrefix() {
		return this.str_GlycoAbunPrefix;
	}
}
