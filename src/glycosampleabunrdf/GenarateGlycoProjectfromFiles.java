package glycosampleabunrdf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.*;

import com.github.andrewoma.dexx.collection.HashMap;
import com.github.andrewoma.dexx.collection.Map;

public class GenarateGlycoProjectfromFiles {

	/*
	 *
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Start RDF generation ...");
		// if GP_x, add GA_ and GS_ IDs
		LinkedList<GlycoProject> ar_Project = new LinkedList<GlycoProject>();
		// AH-20
		// Disease: AH-19 cancer
		// Normal:  AH-18 normal
		Boolean bL_test = false;
		bL_test = false;
		String data_ext = ".pro";

		String t_strFilePath = "";
		String t_strDirPath = "/Users/yamada/Desktop/BH18.7/GlycoNAVI_RDF/GD/";
		//t_strFilePath += "Disease_PMID18214858_三善先生_膵臓癌.txt";
		//t_strFilePath = t_strDirPath + "Disease_PMID- 16385567_三善先生_膵臓癌ほか.txt";
		String t_strRDFDirPath = "/Users/yamada/Desktop/BH18.7/GlycoNAVI_RDF/RDF/";

		for (int i=0; i<args.length; ++i) {
			if ("-f".equals(args[i])) {
				t_strDirPath = args[++i];
			}
		}

		Prefix pre = new Prefix();

        Date dt = new Date();
        SimpleDateFormat dtf = new SimpleDateFormat("_yyyy-MM-dd");
        String dtStr = dtf.format(dt);

    	// Setting for Write ttl file of GlycoSample
    	String GlycoProjectPath = t_strRDFDirPath + "GlycoProject" + dtStr + ".ttl";
        FileWriter writeGPfile = new FileWriter(GlycoProjectPath, true);
        PrintWriter pwGP = new PrintWriter(new BufferedWriter(writeGPfile));

		GlycoProject o_gp = new GlycoProject("");

		// create an empty Model
		Model gp_model = ModelFactory.createDefaultModel();

		// add Prefix
		gp_model.setNsPrefix(PrefixTerm.DS.getPrefix(), PrefixTerm.DS.getUri());
		gp_model.setNsPrefix(PrefixTerm.GP.getPrefix(), PrefixTerm.GP.getUri());
		gp_model.setNsPrefix(PrefixTerm.SIO.getPrefix(), PrefixTerm.SIO.getUri());
		gp_model.setNsPrefix(PrefixTerm.OBO.getPrefix(), PrefixTerm.OBO.getUri());
		gp_model.setNsPrefix(PrefixTerm.GA.getPrefix(), PrefixTerm.GA.getUri());
		gp_model.setNsPrefix(PrefixTerm.SKOS.getPrefix(), PrefixTerm.SKOS.getUri());
		gp_model.setNsPrefix(PrefixTerm.RDFS.getPrefix(), PrefixTerm.RDFS.getUri());
		gp_model.setNsPrefix(PrefixTerm.RDF.getPrefix(), PrefixTerm.RDF.getUri());
		gp_model.setNsPrefix(PrefixTerm.DCTERMS.getPrefix(), PrefixTerm.DCTERMS.getUri());
		//gp_model.setNsPrefix(PrefixTerm.DC.getPrefix(), PrefixTerm.DC.getUri());
		gp_model.setNsPrefix(PrefixTerm.GN.getPrefix(), PrefixTerm.GN.getUri());


	try {
		File dir = new File(t_strDirPath);
	    File[] files = dir.listFiles();


	    for (File file : files ) {
			if (file.exists()) {
				Path t_path = file.toPath();
				String t_fileName = t_path.getFileName().toString();
				//System.out.println(t_path);
				String t_ext = "";
				if (t_fileName.length() > 3) {
					t_ext = t_fileName.substring(t_fileName.length() - 4, t_fileName.length());
				}
				//System.out.println(t_ext);
				if(t_fileName != null && t_ext.equals(data_ext)) {
					//System.out.println("file exist.");
					String t_filePath = file.toPath().toString();
					System.out.println(t_filePath);
					DataFileReader reader = new DataFileReader(t_filePath);
					String str_FileStrings = reader.getFileStrings();
					//str_FileStrings = util.replaceCharacter(str_FileStrings);

/*
					// create an empty Model
					Model gp_model = ModelFactory.createDefaultModel();

					// add Prefix
					gp_model.setNsPrefix(PrefixTerm.GS.getPrefix(), PrefixTerm.GS.getUri());
					gp_model.setNsPrefix(PrefixTerm.GP.getPrefix(), PrefixTerm.GP.getUri());
					gp_model.setNsPrefix(PrefixTerm.SIO.getPrefix(), PrefixTerm.SIO.getUri());
					gp_model.setNsPrefix(PrefixTerm.OBO.getPrefix(), PrefixTerm.OBO.getUri());
					gp_model.setNsPrefix(PrefixTerm.GA.getPrefix(), PrefixTerm.GA.getUri());
					gp_model.setNsPrefix(PrefixTerm.SKOS.getPrefix(), PrefixTerm.SKOS.getUri());
					gp_model.setNsPrefix(PrefixTerm.RDFS.getPrefix(), PrefixTerm.RDFS.getUri());
					gp_model.setNsPrefix(PrefixTerm.RDF.getPrefix(), PrefixTerm.RDF.getUri());
					gp_model.setNsPrefix(PrefixTerm.DCTERMS.getPrefix(), PrefixTerm.DCTERMS.getUri());
					gp_model.setNsPrefix(PrefixTerm.GN.getPrefix(), PrefixTerm.GN.getUri());
*/



					// split string: source, sample, abundance
					//DataSplit datasp = new DataSplit();
					String[] lines = str_FileStrings.split("\n");


					int count = 0;
					for (String line : lines) {
						count++;
						if (count > 1) {


						o_gp = new GlycoProject(line);
						ar_Project.add(o_gp);

						String EntityResource = PrefixTerm.GP.getUri() + o_gp.get_glycoProjectId();

						// create the resource
						Resource glycoproject = gp_model.createResource(EntityResource);

						// add the property
						String t_project_type = ClassUri.Project.getClassUri();
						Resource r_project_type = gp_model.createResource(t_project_type);
						glycoproject.addProperty(RDF.type, r_project_type);
						glycoproject.addProperty(DCTerms.identifier, o_gp.get_glycoProjectId());
						glycoproject.addProperty(RDFS.label, o_gp.getGlycoProjectIdString());
						glycoproject.addProperty(gp_model.createProperty(PredicateTerms.altLabel.getPredicate()), o_gp.get_localId());
						//glycoproject.addProperty(gp_model.createProperty(PredicateTerms.note.getPredicate()), o_gp.getProject_title());
						glycoproject.addProperty(DCTerms.title, o_gp.getProject_title());
						glycoproject.addProperty(DCTerms.description, o_gp.getProject_description());

						if (o_gp.getGlycoProjectIds() != null) {
							String[] localIds = o_gp.getGlycoProjectIds();
							for (String id : localIds) {

								if (id.contains("GP")) {
									String t_prolocalIdUri = PrefixTerm.GP.getUri() + id;
									Resource t_prolocalIdRes = gp_model.createResource(t_prolocalIdUri);
									glycoproject.addProperty(gp_model.createProperty(PredicateTerms.has_attribute.getPredicate()), t_prolocalIdRes);

									String t_pro = ClassUri.Project.getClassUri();
									Resource r_pro_type = gp_model.createResource(t_pro);
									t_prolocalIdRes.addProperty(RDF.type, r_pro_type);
									t_prolocalIdRes.addProperty(DCTerms.identifier, id);

									for (GlycoProject gProj : ar_Project){
										if (gProj.get_glycoProjectId().equals(id)) {
											// add resource GA and GS
										}
									}

								}
								else {
									String t_abunlocalIdUri = PrefixTerm.GA.getUri() + id;
									Resource t_abunlocalIdRes = gp_model.createResource(t_abunlocalIdUri);
									glycoproject.addProperty(gp_model.createProperty(PredicateTerms.has_attribute.getPredicate()), t_abunlocalIdRes);

									String t_abun = ClassUri.Abundance.getClassUri();
									Resource r_abun_type = gp_model.createResource(t_abun);
									t_abunlocalIdRes.addProperty(RDF.type, r_abun_type);
									t_abunlocalIdRes.addProperty(DCTerms.identifier, id);

									String t_samplocalIdUri = PrefixTerm.DS.getUri() + id;
									Resource t_samplocalIdRes = gp_model.createResource(t_samplocalIdUri);
									glycoproject.addProperty(gp_model.createProperty(PredicateTerms.has_attribute.getPredicate()), t_samplocalIdRes);

									String t_samp = ClassUri.Sample.getClassUri();
									Resource r_samp_type = gp_model.createResource(t_samp);
									t_samplocalIdRes.addProperty(RDF.type, r_samp_type);
									t_samplocalIdRes.addProperty(DCTerms.identifier, id);
								}

							}
						}



						// https://jena.apache.org/documentation/io/rdf-output.html
						gp_model.write(pwGP, "TTL");
						}

					} // entities
				} // file is *.txt
			} // file.exists
		} // for files

			// close PrintWriter
			pwGP.close();


		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Fin...!");
	}
}
