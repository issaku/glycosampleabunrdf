package glycosampleabunrdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
//import java.util.HashMap;
//import java.util.LinkedList;
import glycosampleabunrdf.util;

public class DataFileReader {

	private String m_strOutput;

	public DataFileReader(final String a_objFilepath) {
		StringBuilder sb = new StringBuilder();
		try{
			  File file = new File(a_objFilepath);
			  BufferedReader br = new BufferedReader(new FileReader(file));
			  String str;
			  while((str = br.readLine()) != null){
			    //System.out.println(str);
				  /*
				  String strline0 = str.replaceAll("\\t", "");
				  String strline1 = strline0.replaceAll("\\r\\n", "");
				  String strline = strline1.replaceAll("\\n", "");
				  */
				  String strline = util.removeTabReturn(str);

				  //String strline = str.replaceAll("(\\r\\n)+", "");
				  //String strline = str.replaceAll("(?m)ˆ\\s*$[\n\r]{1,}", "");
			    if (strline.getBytes().length > 0) {
			    	//System.out.println(strline);
			    	//System.out.println(str);
			    	//System.out.println("String Length: "+ strline.length());
			    	sb.append(str + "\n");
			    }
			  }

			  br.close();
			}catch(FileNotFoundException e){
			  System.out.println(e);
			}catch(IOException e){
			  System.out.println(e);
			}
		this.m_strOutput = sb.toString();
	}

	public String getFileStrings() {
		return this.m_strOutput;
	}

}

