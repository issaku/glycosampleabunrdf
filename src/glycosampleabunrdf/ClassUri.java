package glycosampleabunrdf;

public enum ClassUri {


		// GlycoCoO
		Relative_abundance    ("http://purl.jp/bio/12/glyco/conjugate#Relative_abundance"),

		// UniProt
		Protein          ("http://purl.uniprot.org/core/Protein"),


		// GlycoNAVI
		DB_memo          ("http://purl.jp/bio/12/glyco/glyconavi#DB_memo"),
		Project          ("http://purl.jp/bio/12/glyco/glyconavi#Project"),
		Sample          ("http://purl.jp/bio/12/glyco/glyconavi#Sample"),
		Abundance          ("http://purl.jp/bio/12/glyco/glyconavi#Abundance"),


		// CODAO
		CompoundDiseaseAssosiation      ("http://purl.glycoinfo.org/ontology/codao#CompoundDiseaseAssosiation"),

		// GlycoRDF
		Source_natural     				("http://purl.jp/bio/12/glyco/glycan#Source_natural"),
		Saccharide     				("http://purl.jp/bio/12/glyco/glycan#Saccharide"),
		Glycoprotein     				("http://purl.jp/bio/12/glyco/glycan#Glycoprotein"),
		Glycosylation_Site     				("http://purl.jp/bio/12/glyco/glycan#Glycosylation_site"),
		Glycosylated_AA     				("http://purl.jp/bio/12/glyco/glycan#Glycosylated_AA"),

		// FALDO
		ExactPosition       ("http://biohackathon.org/resource/faldo#ExactPosition"),
		FuzzyPosition       ("http://biohackathon.org/resource/faldo#FuzzyPosition"),


		// SIO
		Set         ("http://semanticscience.org/resource/SIO_000289"), // sio:set
		SetItem         ("http://semanticscience.org/resource/SIO_001258"), // sio:setitem
		Disease      ("http://semanticscience.org/resource/SIO_010299"),

		Taxon							("http://purl.uniprot.org/core/Taxon"),
		Citation      	("http://purl.jp/bio/12/glyco/glycan#Citation");


		private String classUri;

		public String getClassUri() {
			return this.classUri;
		}

		ClassUri(String a_class){
			this.classUri = a_class;
		}

}
