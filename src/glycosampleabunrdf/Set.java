package glycosampleabunrdf;

import java.util.LinkedList;

public class Set {

	LinkedList<Abundance> set = new LinkedList<Abundance>();

	public Set(Abundance a_abun) {
		this.set.add(a_abun);
	}

	public LinkedList<Abundance> getSet() {
		return this.set;
	}

	public void setSet(Abundance a_abun) {
		this.set.add(a_abun);
	}

	public void clear(){
		this.set.clear();
	}
}
