package glycosampleabunrdf;

public enum PrefixTerm {

	GCO         ("http://purl.jp/bio/12/glyco/conjugate#",                 "gco"),
	DCTERMS     ("http://purl.org/dc/terms/",                      "dcterms"),
	//DC         ("http://purl.org/dc/elements/1.1/",                "dc"),
	XSD         ("http://www.w3.org/2001/XMLSchema#",              "xsd"),
	RDFS        ("http://www.w3.org/2000/01/rdf-schema#",          "rdfs"),
	RDF         ("http://www.w3.org/1999/02/22-rdf-syntax-ns#",    "rdf"),
	FABIO       ("http://purl.org/spar/fabio/",                    "fabio"),
	UP          ("http://purl.uniprot.org/core/",                  "up"),
	// https://www.uniprot.org/uniprot/Q966M4
	UNIPROT     ("http://www.uniprot.org/uniprot/",                  "uniprot"),
	SIO         ("http://semanticscience.org/resource/",           "sio"),
	OBO         ("http://purl.obolibrary.org/obo/",                "obo"),
	EDAM        ("http://edamontology.org/",                       "edam"),
	GN          ("http://purl.jp/bio/12/glyco/glyconavi#", "gn"),
	GD          ("http://purl.jp/bio/12/glyco/glyconavi/disease#",  "gd"),
	DS          ("http://purl.jp/bio/12/glyco/glyconavi/DiseaseSample#",  "gs"),
	GA          ("http://purl.jp/bio/12/glyco/glyconavi/DiseaseAbun#",    "ga"),
	GP        ("http://purl.jp/bio/12/glyco/glyconavi/DiseaseProject#", "gp"),
	GPSS        ("http://purl.jp/bio/12/glyco/glyconavi/DiseaseProject#SampleSet", "gpss"),
	//SampleSet_
	SET          ("http://purl.jp/bio/12/glyco/glyconavi/abun#set",    "set"),
	SETITEM     ("http://purl.jp/bio/12/glyco/glyconavi/abun#setitem",    "setitem"),
	GLYCAN      ("http://purl.jp/bio/12/glyco/glycan#",            "glycan"),
	GTCI        ("http://identifiers.org/glytoucan/",             "gtci"),
	GTC         ("http://glytoucan.org/Structures/Glycans/",       "gtc"),
	FOAF        ("http://xmlns.com/foaf/0.1/",                     "foaf"),
	FALDO       ("http://biohackathon.org/resource/faldo#",        "faldo"),
	CODAO       ("http://purl.glycoinfo.org/ontology/codao#",      "codao"),
	UO          ("http://purl.obolibrary.org/obo/uo#",             "UO"),
	SKOS        ("http://www.w3.org/2004/02/skos/core#",           "skos"),
	pubmed		("http://www.ncbi.nlm.nih.gov/pubmed/",			"pubmed"),
	id_pubmed		("http://identifiers.org/pubmed/",			"id_pubmed");



	private String uri;
	private String prefix;

	public String getUri() {
		return this.uri;
	}

	public String getPrefix() {
		return this.prefix;
	}

	PrefixTerm(String a_uri, String a_prefix){
		this.uri = a_uri;
		this.prefix = a_prefix;
	}

}
